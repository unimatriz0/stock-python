#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Nombre: financiamiento.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 02/02/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnostico
    Comentarios: Clase que controla las operaciones sobre la tabla
                 de fuentes de financiamiento

"""

# importamos la clase de base de datos
from clases.dbapi import Conexion


class Financiamiento:
    """Definición de la clase"""

    def __init__(self):
        ' constructor de la clase '

        # declaración de variables de clase
        self.Id = 0                # clave del registro
        self.Fuente = ""           # nombre de la fuente
        self.IdLaboratorio = 0     # clave del laboratorio
        self.IdUsuario = 0         # clave del usuario
        self.Usuario = ""          # nombre del usuario
        self.FechaAlta = ""        # fecha de alta del registro

        # instanciamos la conexión
        self.Link = Conexion()

    def nominaFinanciamiento(self):
        """ Método que retorna un array con la nómina de
            fuentes de financiamiento """

        # componemos la consulta
        consulta = ("SELECT diagnostico.v_financiamiento.id_financiamiento AS id, "
                    "       diagnostico.v_financiamiento.fuente AS fuente, "
                    "       diagnostico.v_financiamiento.usuario AS usuario, "
                    "       diagnostico.v_financiamiento.fecha_alta AS fecha_alta "
                    "FROM diagnostico.v_financiamiento "
                    "WHERE diagnostico.v_financiamiento.id_laboratorio = %s "
                    "ORDER BY diagnostico.v_financiamiento.fuente;")

        # asignamos los parámetros y ejecutamos la consulta
        parametros = (self.IdLaboratorio, )
        resultado = self.Link.consultar(consulta, parametros)

        # retornamos el vector
        return resultado

    def getDatosFinanciamiento(self, idregistro):
        """ Método que recibe como parámetro la clave de un registro y
            asigna los valores en las variables de clase """

        # componemos la consulta y asignamos los parámetros
        consulta = ("SELECT diagnostico.v_financiamiento.id_financiamiento AS id, "
                    "       diagnostico.v_financiamiento.fuente AS fuente, "
                    "       diagnostico.v_financiamiento.usuario AS usuario, "
                    "       diagnostico.v_financiamiento.fecha_alta AS fecha_alta "
                    "FROM diagnostico.v_financiamiento "
                    "WHERE diagnostico.v_financiamiento.id_financiamiento = %s; ")
        parametros = (idregistro, )

        # obtenemos el registro y lo asignamos a las variables de clase
        resultado = self.Link.obtener(consulta, parametros)
        self.Id = resultado["id"]
        self.Fuente = resultado["fuente"]
        self.Usuario = resultado["usuario"]
        self.FechaAlta = resultado["fecha_alta"]

    def getClaveFinanciamiento(self, financiamiento):
        """ Método que recibe una cadena y retorna la
            clave de la fuente de financiamiento

            Parámetros: financiamiento - cadena con el nombre
            Retorna: id - entero con la clave """

        # componemos la consulta
        consulta = ("SELECT diagnostico.financiamiento.id AS id "
                    "FROM diagnostico.financiamiento "
                    "WHERE diagnostico.financiamiento.fuente = %s AND "
                    "      diagnostico.financiamiento.id_laboratorio = %s; ")

        # asignamos los parámetros y obtenemos el registro
        parametros = (financiamiento, self.IdLaboratorio)
        resultado = self.Link.obtener(consulta, parametros)
        return resultado["id"]

    def grabaFinanciamiento(self):
        """ Método que según corresponda ejecuta la consulta
            de inserción o edición """

        # si está insertando
        if self.Id == 0:
            self.nuevoFinanciamiento()
        else:
            self.editaFinanciamiento()

        # retornamos la clave del registro
        return self.Id

    def nuevoFinanciamiento(self):
        """ Método que ejecuta la consulta de inserción """

        # componemos la consulta
        consulta = ("INSERT INTO diagnostico.financiamiento "
                    "       (fuente, "
                    "        id_laboratorio, "
                    "        id_usuario) "
                    "       VALUES "
                    "       (%s, %s, %s); ")

        # asignamos los parámetros
        parametros = (self.Fuente, self.IdLaboratorio, self.IdUsuario)

        # ejecutamos la consulta y obtenemos la id
        self.Id = self.Link.ejecutar(consulta, parametros)

    def editaFinanciamiento(self):
        """ Método que ejecuta la consulta de edicion """

        # componemos la consulta
        consulta = ("UPDATE diagnostico.financiamiento SET "
                    "       fuente = %s, "
                    "       id_usuario = %s "
                    "WHERE diagnostico.financiamiento.id = %s; ")

        # asignamos los parámetros y ejecutamos la consulta
        parametros = (self.Fuente, self.IdUsuario, self.Id)
        self.Link.ejecutar(consulta, parametros)

    def validaFinanciamiento(self, financiamiento):
        """ Método que recibe como parámetro el nombre de
            una fuente de financiamiento y verifica que no
            se encuentre repetida """

        # componemos la consulta y asigna los parámetros
        consulta = ("SELECT COUNT(diagnostico.financiamiento.id) AS registros "
                    "FROM diagnostico.financiamiento "
                    "WHERE diagnostico.financiamiento.fuente = %s; ")
        parametros = (financiamiento, )

        # ejecutamos la consulta y según el resultado
        resultado = self.Link.obtener(consulta, parametros)
        if resultado["registros"] == 0:
            return True
        else:
            return False

    def puedeBorrar(self, idfinanciamiento):
        """ Método llamado antes de eliminar un registro,
            recibe como parámetro la clave de una fuente
            de financiamiento y verifica que no tenga hijos """

        # componemos la consulta y asignamos los parámetros
        consulta = ("SELECT COUNT(diagnostico.ingresos.id) AS registros "
                    "FROM diagnostico.ingresos "
                    "WHERE diagnostico.ingresos.id_financiamiento = %s; ")
        parametros = (idfinanciamiento, )

        # ejecutamos la consulta y según el resultado
        resultado = self.Link.obtener(consulta, parametros)
        if resultado["registros"] == 0:
            return True
        else:
            return False

    def borraFinanciamiento(self, idfinanciamiento):
        """ Método que recibe como parámetro la clave de
            una fuente de financiamiento y ejecuta la
            consulta de eliminación """

        # componemos la consulta y asignamos los parámetros
        consulta = ("DELETE FROM diagnostico.financiamiento "
                    "WHERE diagnostico.financiamiento.id = %s;")
        parametros = (idfinanciamiento, )

        # ejecutamos la consulta
        self.Link.ejecutar(consulta, parametros)
