#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Nombre: form_financiamiento.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 03/02/2019
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Comentarios: Clase que arma el formulario con la grilla de
                 fuentes de financiamiento

"""

# para traducir los formularios de ui a py usamos
# pyuic5 input.ui -o output.py

# importamos las librerías
from PyQt5 import QtWidgets, QtGui, QtCore
import time

# importamos las clases
from financiamiento.financiamiento import Financiamiento


class FormFinanciamiento(QtWidgets.QDialog):
    ' Definición de la clase '

    def __init__(self, parent):
        """ definimos el constructor de la clase que recibe
            como parámetro el padre """

        # definimos la variable idUsuario
        self.IdUsuario = 0             # clave del usuario
        self.IdLaboratorio = 0         # clave del laboratorio
        self.Usuario = ""              # nombre del usuario

        # llama al constructor del padre
        super(FormFinanciamiento, self).__init__(parent)

    def setupUi(self):
        """ Método que arma el formulario, asume que ya fueron
            asignadas las propiedades de nombre de usuario e id """

        # redimensionamos y fijamos la posición del formulario
        self.setGeometry(370, 200, 715, 384)

        # fijamos el ícono
        self.setWindowIcon(QtGui.QIcon('recursos/logo_fatala.jpg'))

        # fijamos el título de la ventana
        self.setWindowTitle("Fuentes de financiamiento")

        # establecemos la ventana como modal
        self.setModal(True)

        # define la fuente de los labels
        font = QtGui.QFont()
        font.setPointSize(10)
        font.setBold(True)
        font.setWeight(75)

        # presenta el título
        lTitulo = QtWidgets.QLabel("Seleccione la fuente de financiamiento de la grilla", self)
        lTitulo.setGeometry(10, 10, 641, 24)
        lTitulo.setFont(font)

        # presenta la clave
        lId = QtWidgets.QLabel("ID:", self)
        lId.setGeometry(11, 40, 20, 14)
        lId.setFont(font)
        self.tId = QtWidgets.QLineEdit(self)
        self.tId.setToolTip("Clave del registro")
        self.tId.setGeometry(34, 36, 40, 24)
        self.tId.setReadOnly(True)

        # presenta la fuente de financiamiento
        lFinanciamiento = QtWidgets.QLabel("Fuente", self)
        lFinanciamiento.setGeometry(90, 40, 57, 14)
        lFinanciamiento.setFont(font)
        self.tFinanciamiento = QtWidgets.QLineEdit(self)
        self.tFinanciamiento.setGeometry(149, 36, 220, 24)
        self.tFinanciamiento.setToolTip("Nombre de la fuente de financiamiento")

        # presenta el usuario
        lUsuario = QtWidgets.QLabel("Usuario:", self)
        lUsuario.setGeometry(380, 40, 57, 14)
        lUsuario.setFont(font)
        self.tUsuario = QtWidgets.QLineEdit(self.Usuario, self)
        self.tUsuario.setGeometry(440, 35, 70, 24)
        self.tUsuario.setToolTip("Usuario que ingresó el registro")
        self.tUsuario.setReadOnly(True)

        # presenta la fecha de alta
        lAlta = QtWidgets.QLabel("Alta:", self)
        lAlta.setGeometry(520, 40, 40, 14)
        lAlta.setFont(font)
        self.tAlta = QtWidgets.QLineEdit(self)
        self.tAlta.setGeometry(557, 35, 80, 24)
        self.tAlta.setToolTip("Fecha de alta del registro")
        self.tAlta.setReadOnly(True)

        # fijamos el valor por defecto
        self.tAlta.setText(time.strftime("%d/%m/%Y"))

        # presenta el botón grabar
        btnGrabar = QtWidgets.QPushButton(self)
        btnGrabar.setGeometry(640, 34, 24, 24)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("recursos/grabar.png"))
        btnGrabar.setIcon(icon)
        btnGrabar.setToolTip("Pulse para grabar el registro")

        # fija el evento del botón
        btnGrabar.clicked.connect(self.validaFuente)

        # presenta el botón cancelar
        btnCancelar = QtWidgets.QPushButton(self)
        btnCancelar.setGeometry(672, 34, 24, 24)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("recursos/90.png"))
        btnCancelar.setIcon(icon1)
        btnCancelar.setToolTip("Reinicia el formulario")

        # fija el evento del botón
        btnCancelar.clicked.connect(self.limpiaFormulario)

        # presenta la tabla
        self.tFuentes = QtWidgets.QTableWidget(self)
        self.tFuentes.setGeometry(10, 70, 691, 301)
        self.tFuentes.setRowCount(0)
        self.tFuentes.setColumnCount(6)
        self.tFuentes.setHorizontalHeaderLabels(('ID',
                                                 'Fuente',
                                                 'Usuario',
                                                 'Alta',
                                                 'Ed.',
                                                 'El.'))

        # fijamos los tooltips de las columnas
        self.tFuentes.horizontalHeaderItem(4).setToolTip("Edita el registro")
        self.tFuentes.horizontalHeaderItem(5).setToolTip("Elimina el registro")

        # fijamos la alineación de las columnas
        self.tFuentes.horizontalHeaderItem(0).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tFuentes.horizontalHeaderItem(1).setTextAlignment(QtCore.Qt.AlignLeft)
        self.tFuentes.horizontalHeaderItem(2).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tFuentes.horizontalHeaderItem(3).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tFuentes.horizontalHeaderItem(4).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tFuentes.horizontalHeaderItem(5).setTextAlignment(QtCore.Qt.AlignCenter)

        # fijamos el ancho de las columnas
        self.tFuentes.setColumnWidth(0, 30)
        self.tFuentes.setColumnWidth(1, 380)
        self.tFuentes.setColumnWidth(2, 100)
        self.tFuentes.setColumnWidth(3, 100)
        self.tFuentes.setColumnWidth(4, 30)
        self.tFuentes.setColumnWidth(5, 30)

        # permitimos reordenar
        self.tFuentes.setSortingEnabled(True)

        # instanciamos la clase de la base de datos
        self.Fuentes = Financiamiento()

        # asignamos las propiedades de la clase
        self.Fuentes.IdLaboratorio = self.IdLaboratorio
        self.Fuentes.IdUsuario = self.IdUsuario

        # define los eventos de la grilla
        self.tFuentes.cellClicked.connect(self.accionFuente)

        # carga la nómina de fuentes
        self.cargaGrilla()

        # muestra el formulario
        self.show()

    def cargaGrilla(self):
        """ Método que carga la grilla de fuentes de financiamiento """

        # obtenemos la nómina de fuentes
        nomina = self.Fuentes.nominaFinanciamiento()

        # recorremos el vecot
        for registro in nomina:

            # insertamos una fila
            fila = self.tFuentes.rowCount()
            self.tFuentes.insertRow(fila)

            # fijamos los valores convirtiendo las cadenas
            self.tFuentes.setItem(fila, 0, QtWidgets.QTableWidgetItem(str(registro["id"])))
            self.tFuentes.setItem(fila, 1, QtWidgets.QTableWidgetItem(registro["fuente"]))
            self.tFuentes.setItem(fila, 2, QtWidgets.QTableWidgetItem(registro["usuario"]))
            self.tFuentes.setItem(fila, 3, QtWidgets.QTableWidgetItem(registro["fecha_alta"]))

            # definimos el ícono de edición
            icon = QtGui.QIcon(QtGui.QPixmap("recursos/13.png"))
            item = QtWidgets.QTableWidgetItem(icon, "")
            self.tFuentes.setItem(fila, 4, item)

            # define el ícono de borrar
            icon = QtGui.QIcon(QtGui.QPixmap("recursos/90.png"))
            item = QtWidgets.QTableWidgetItem(icon, "")
            self.tFuentes.setItem(fila, 5, item)

    def limpiaFormulario(self):
        """ Método que limpia el formulario de carga de datos """

        # limpiamos los componentes
        self.tId.setText("")
        self.tFinanciamiento.setText("")
        self.tUsuario.setText(self.Usuario)
        self.tAlta.setText(time.strftime("%d/%m/%Y"))

        # fijamos el foco
        self.tFinanciamiento.setFocus()

    def cargaFuente(self, idfuente):
        """ Método que recibe como parámetro la clave de una fuente de
            financiamiento y carga los datos en el formulario """

        # obtenemos los datos del registro
        self.Fuentes.getDatosFinanciamiento(idfuente)

        # asignamos en el formulario
        self.tId.setText(str(self.Fuentes.Id))
        self.tFinanciamiento.setText(self.Fuentes.Fuente)
        self.tUsuario.setText(self.Fuentes.Usuario)
        self.tAlta.setText(self.Fuentes.FechaAlta)

        # fijamos el foco
        self.tFinanciamiento.setFocus()

    def borraFuente(self, idfuente):
        """ Método que recibe como parámetro la clave de una fuente y
            ejecuta la consulta de eliminación """

        # verifica si puede borrar
        if not self.Fuentes.puedeBorrar(idfuente):

            # presenta el mensaje y retorna
            titulo = 'Atención'
            mensaje = "Ese financiamiento está asignado\n"
            mensaje += "a una compra, no se puede eliminar"
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # pide confirmación
        titulo = "Confirmación"
        mensaje = "Está seguro que desea\n"
        mensaje += "eliminar el registro?"
        respuesta = QtWidgets.QMessageBox.question(self,
                                         titulo,
                                         mensaje,
                                         QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)

        # si confirmó
        if respuesta == QtWidgets.QMessageBox.Yes:

            # elimina el registro
            self.Fuentes.borraFinanciamiento(idfuente)

            # limpia y recarga la grilla
            self.tFuentes.setRowCount(0)
            self.cargaGrilla()

    def validaFuente(self):
        """ Método que verifica el formulario antes de enviarlo a
            grabar """

        # verifica que halla ingresado la fuente
        if self.tFinanciamiento.text() == "":

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = "Indique el nombre del financiamiento"
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # si está insertando
        if self.tId.text() == "":

            # verifica que no esté repetido
            if not self.Fuentes.validaFinanciamiento(self.tFinanciamiento.text()):

                # presenta el mensaje
                titulo = 'Atención'
                mensaje = "Esa fuente ya está declarada"
                QtWidgets.QMessageBox.warning(self, titulo, mensaje)
                return

        # si llegó hasta aquí
        self.grabaFuente()

    def grabaFuente(self):
        """ Método que ejecuta la consulta de grabación en la base
            de datos """

        # asigna los valores en la clase asegurándonos
        # si está editando que la clave sea cero
        if self.tId.text() != "":
            self.Fuentes.Id = self.tId.text()
        else:
            self.Fuentes.Id = 0
        self.Fuentes.Fuente = self.tFinanciamiento.text()
        self.Fuentes.IdUsuario = self.IdUsuario
        self.Fuentes.IdLaboratorio = self.IdLaboratorio

        # ejecuta la consulta y obtiene la id
        id = self.Fuentes.grabaFinanciamiento()

        # si está insertando
        if self.tId.text == "":

            # agregamos la fila
            self.nuevoRegistro(id)

        # si está editando
        else:

            # limpiamos la tabla
            self.tFuentes.setRowCount(0)

            # recarga la grilla
            self.cargaGrilla()

        # limpia el formulario
        self.limpiaFormulario()

    def nuevoRegistro(self, idfinanciamiento):
        """ Método llamado luego de insertar un registro, recibe
            la clave e inserta los valores del formulario """

        # insertamos una fila
        fila = self.tFuentes.rowCount()
        self.tFuentes.insertRow(fila)

        # fijamos los valores convirtiendo las cadenas
        self.tFuentes.setItem(fila, 0, QtWidgets.QTableWidgetItem(str(id)))
        self.tFuentes.setItem(fila, 1, QtWidgets.QTableWidgetItem(self.tFinanciamiento.text()))
        self.tFuentes.setItem(fila, 2, QtWidgets.QTableWidgetItem(self.Usuario))
        self.tFuentes.setItem(fila, 3, QtWidgets.QTableWidgetItem(time.strftime("%d/%m/%Y")))

        # definimos el ícono de edición
        icon = QtGui.QIcon(QtGui.QPixmap("recursos/13.png"))
        item = QtWidgets.QTableWidgetItem(icon, "")
        self.tFuentes.setItem(fila, 4, item)

        # define el ícono de borrar
        icon = QtGui.QIcon(QtGui.QPixmap("recursos/90.png"))
        item = QtWidgets.QTableWidgetItem(icon, "")
        self.tFuentes.setItem(fila, 5, item)

    # evento llamado al pulsar sobre un registro en la grilla
    def accionFuente(self, fila, columna):
        """ Este evento recibe como parámetro la fila y
            columna que se pulsó en la grilla """

        # obtenemos el valor de la celda
        idfuente = self.tFuentes.item(fila, 0).text()

        # si pulsó sobre editar
        if columna == 4:

            # presentamos el registro
            self.cargaFuente(idfuente)

        # si pulsó borrar
        elif columna == 5:

            # eliminamos el registro
            self.borraFuente(idfuente)