#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Nombre: ingresos.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 01/02/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnostico
    Comentarios: Clase que contiene los metodos para operar con las tablas de
                 ingresos de materiales del control de stock

"""

# importamos la librería
from clases.dbapi import Conexion


class Ingresos:
    ' Definición de la clase '

    def __init__(self):
        ' Constructor de la clase '

        # constantes de la clase que identifican al usuario
        self.ID = ""                # clave del usuario
        self.IDLABORATORIO = ""     # clave del laboratorio

        # variables públicas de la tabla de ingresos
        self.IdIngreso = 0              # clave del registro
        self.ItemIngreso = ""           # nombre del artículo
        self.IdItemIngreso = 0          # clave del artículo
        self.CodigoSop = ""             # código sop del artículo
        self.Lote = ""                  # número de lote
        self.Imagen = ""                # imagen del artículo
        self.ImporteIngreso = ""        # importe de la factura
        self.RemitoIngreso = ""         # remito del ingreso
        self.CantidadIngreso = 0        # cantidad ingresada
        self.Financiamiento = ""        # fuente de financiamiento
        self.IdFinanciamiento = 0       # clave del financiamiento
        self.VencimientoIngreso = ""    # fecha de vencimiento
        self.FechaIngreso = ""          # fecha del ingreso
        self.Ubicacion = ""             # lugar de almacenamiento
        self.Usuario = ""               # nombre del usuario
        self.IdUsuario = 0              # clave del usuario
        self.ComentariosIngreso = ""    # comentarios del usuario

        # instanciamos la conexión
        self.Link = Conexion()

    def listaIngresos(self):
        """ Método que lista todas las entradas al depósito del
            laboratorio activo

            Retorna: array con las entradas """

        consulta = ("SELECT diagnostico.v_ingresos.idingreso AS idingreso, "
                    "       diagnostico.v_ingresos.descripcion AS descripcion, "
                    "       diagnostico.v_ingresos.iditem AS iditem, "
                    "       diagnostico.v_ingresos.remito AS remito, "
                    "       diagnostico.v_ingresos.codigosop AS codigosop, "
                    "       diagnostico.v_ingresos.imagen AS imagen, "
                    "       diagnostico.v_ingresos.id_financiamiento AS idfinanciamiento, "
                    "       diagnostico.v_ingresos.financiamiento AS financiamiento, "
                    "       diagnostico.v_ingresos.cantidad AS cantidad, "
                    "       diagnostico.v_ingresos.usuario AS recibio, "
                    "       diagnostico.v_ingresos.vencimiento AS vencimiento, "
                    "       diagnostico.v_ingresos.fecha_ingreso AS fecha_ingreso, "
                    "       diagnostico.v_ingresos.lote AS lote, "
                    "       diagnostico.v_ingresos.ubicacion AS ubicacion "
                    "FROM diagnostico.v_ingresos "
                    "WHERE diagnostico.v_ingresos.idlaboratorio = %s "
                    "ORDER BY STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%%d/%%m/%%Y') DESC, "
                    "         diagnostico.v_ingresos.descripcion;")

        # asignamos los parámetros
        parametros = (self.IDLABORATORIO, )

        # ejecutamos la consulta y retornamos el vector
        resultado = self.Link.consultar(consulta, parametros)
        return resultado

    def listaIngresoFecha(self, inicio, fin):
        """Método que lista las entradas al depósito en un cierto período
           de tiempo, recibe como parámetros las fechas de inicio y fin
           en formato dd/mm/YYYY

           Parámetros: inicio - string con la fecha de inicio
                       fin - string con la fecha de finalización

           Retorna: array con los resultados """

        # componemos la consulta de los ingresos solo para el laboratorio
        # del usuario en el período indicado
        consulta = ("SELECT diagnostico.v_ingresos.idingreso AS idingreso, "
                    "       diagnostico.v_ingresos.descripcion AS descripcion, "
                    "       diagnostico.v_ingresos.iditem AS iditem, "
                    "       diagnostico.v_ingresos.remito AS remito, "
                    "       diagnostico.v_ingresos.codigosop AS codigosop, "
                    "       diagnostico.v_ingresos.imagen AS imagen, "
                    "       diagnostico.v_ingresos.id_financiamiento AS idfinanciamiento, "
                    "       diagnostico.v_ingresos.financiamiento AS financiamiento, "
                    "       diagnostico.v_ingresos.cantidad AS cantidad, "
                    "       diagnostico.v_ingresos.usuario AS recibio, "
                    "       diagnostico.v_ingresos.vencimiento AS vencimiento, "
                    "       diagnostico.v_ingresos.fecha_ingreso AS fecha_ingreso, "
                    "       diagnostico.v_ingresos.lote AS lote, "
                    "       diagnostico.v_ingresos.ubicacion AS ubicacion "
                    "FROM diagnostico.v_ingresos "
                    "WHERE diagnostico.v_ingresos.id_laboratorio = %s AND "
                    "      STR_TO_DATE(%s, '%%d/%%m/%%Y') > STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%%d/%%m/%%Y') AND "
                    "      STR_TO_DATE(%s, '%%d/%%m/%%Y') < STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%%d/%%m/%%Y') "
                    "ORDER BY STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%%d/%%m/%%Y') DESC "
                    "         diagnostico.v_ingresos.descripcion;")

        # asignamos los parámetros
        parametros = (self.IDLABORATORIO, inicio, fin)

        # ejecutamos la consulta y retornamos el vector
        resultado = self.Link.consultar(consulta, parametros)
        return resultado

    def listaIngresoItem(self, item):
        """Método que recibe como parámetro la clave de un item y retorna
           las entradas al depósito de ese item

           Parámetros: item - entero con la clave del item
           Retorna: array con los registros encontrados """

        # componemos la consulta de los ingresos solo para el laboratorio
        # del usuario
        consulta = ("SELECT diagnostico.v_ingresos.idingreso AS idingreso, "
                    "       diagnostico.v_ingresos.descripcion AS descripcion, "
                    "       diagnostico.v_ingresos.imagen AS imagen, "
                    "       diagnostico.v_ingresos.codigosop AS codigosop, "
                    "       diagnostico.v_ingresos.importe AS importe, "
                    "       diagnostico.v_ingresos.financiamiento AS financiamiento, "
                    "       diagnostico.v_ingresos.id_financiamiento AS idfinanciamiento, "
                    "       diagnostico.v_ingresos.remito AS remito, "
                    "       diagnostico.v_ingresos.cantidad AS cantidad, "
                    "       diagnostico.v_ingresos.vencimiento AS vencimiento, "
                    "       diagnostico.v_ingresos.fecha_ingreso AS fechaingreso, "
                    "       diagnostico.v_ingresos.lote AS lote, "
                    "       diagnostico.v_ingresos.ubicacion AS ubicacion "
                    "       diagnostico.v_ingresos.usuario AS recibio "
                    "FROM diagnostico.v_ingresos "
                    "WHERE diagnostico.v_ingresos.id_laboratorio = %s AND "
                    "      diagnostico.v_ingresos.iditem = '%s' "
                    "ORDER BY STR_TO_DATE(diagnostico.v_ingresos.fecha_ingreso, '%%d/%%m/%%Y') DESC "
                    "         diagnostico.v_ingresos.descripcion;")

        # asignamos los parámetros
        parametros = (self.IDLABORATORIO, item)

        # ejecutamos la consulta y retornamos el vector
        resultado = self.Link.consultar(consulta, parametros)
        return resultado

    def getDatosIngreso(self, idingreso):
        """Método que recibe como parámetro la clave de un ingreso
           y retorna el vector con los datos del registro """

        # compone la consulta
        consulta = ("SELECT diagnostico.v_ingresos.id_ingreso AS idingreso, "
                    "       diagnostico.v_ingresos.item AS item, "
                    "       diagnostico.v_ingresos.iditem AS iditem, "
                    "       diagnostico.v_ingresos.imagen AS imagen, "
                    "       diagnostico.v_ingresos.codigosop AS codigosop, "
                    "       diagnostico.v_ingresos.importe AS importe, "
                    "       diagnostico.v_ingresos.id_financiamiento AS idfinanciamiento, "
                    "       diagnostico.v_ingresos.financiamiento As financiamiento, "
                    "       diagnostico.v_ingresos.lote AS lote, "
                    "       diagnostico.v_ingresos.ubicacion AS ubicacion, "
                    "       diagnostico.v_ingresos.remito AS remito, "
                    "       diagnostico.v_ingresos.cantidad AS cantidad, "
                    "       diagnostico.v_ingresos.vencimiento AS vencimiento, "
                    "       diagnostico.v_ingresos.fecha_ingreso AS fecha_ingreso, "
                    "       diagnostico.v_ingresos.usuario AS usuario, "
                    "       diagnostico.v_ingresos.idusuario AS idusuario, "
                    "       diagnostico.v_ingresos.comentarios AS comentarios "
                    "FROM diagnostico.v_ingresos "
                    "WHERE diagnostico.v_ingresos.id_ingreso = %s;")

        # asignamos los parámetros
        parametros = (idingreso, )

        # ejecutamos la consulta
        resultado = self.Link.obtener(consulta, parametros)

        # asignamos en las variables de clase
        self.IdIngreso = resultado["idingreso"]
        self.ItemIngreso = resultado["item"]
        self.IdItemIngreso = resultado["iditem"]
        self.Imagen = resultado["imagen"]
        self.CodigoSop = resultado["codigosop"]
        self.ImporteIngreso = resultado["importe"]
        self.IdFinanciamiento = resultado["idfinanciamiento"]
        self.Financiamiento = resultado["financiamiento"]
        self.Lote = resultado["lote"]
        self.Ubicacion = resultado["ubicacion"]
        self.RemitoIngreso = resultado["remito"]
        self.CantidadIngreso = resultado["cantidad"]
        self.VencimientoIngreso = resultado["vencimiento"]
        self.FechaIngreso = resultado["fecha_ingreso"]
        self.Usuario = resultado["usuario"]
        self.IdUsuario = resultado["idusuario"]
        self.ComentariosIngreso = resultado["comentarios"]

    def grabaIngreso(self):
        """Método que a partir de las variables de clase ejecuta la consulta
           de inserción o actualización de un ingreso """

        # si esta dando un alta
        if self.IdIngreso == 0:
            self.nuevoIngreso()
        else:
            self.editaIngreso()

        # retornamos la id del registro
        return self.IdIngreso

    def nuevoIngreso(self):
        """ Método llamado en el alta de un nuevo ingreso """

        # componemos la consulta de inserción
        consulta = ("INSERT INTO diagnostico.ingresos "
                    "       (item, "
                    "        importe, "
                    "        id_laboratorio, "
                    "        id_financiamiento, "
                    "        remito, "
                    "        cantidad, "
                    "        vencimiento, "
                    "        lote, "
                    "        ubicacion, "
                    "        id_usuario, "
                    "        comentarios) "
                    "       VALUES "
                    "       (%s, %s, %s, %s, %s, %s, STR_TO_DATE(%s, '%%d/%%m/%%Y'), %s, %s, %s, %s);")

        # asignamos los parametros
        parametros = (self.IdItemIngreso,
                      self.ImporteIngreso,
                      self.IDLABORATORIO,
                      self.IdFinanciamiento,
                      self.RemitoIngreso,
                      self.CantidadIngreso,
                      self.VencimientoIngreso,
                      self.Lote,
                      self.Ubicacion,
                      self.ID,
                      self.ComentariosIngreso)

        # obtenemos la id del ingreso
        self.IdIngreso = self.Link.ejecutar(consulta, parametros)

    def editaIngreso(self):
        """ Método llamado en la edición de un ingreso """

        # compone la consulta de edicion
        consulta = ("UPDATE diagnostico.ingresos SET "
                    "       item = %s, "
                    "       importe = %s, "
                    "       id_laboratorio = %s, "
                    "       id_financiamiento = %s, "
                    "       remito = %s, "
                    "       cantidad = %s, "
                    "       vencimiento = STR_TO_DATE(%s, '%%d/%%m/%%Y'), "
                    "       lote = %s, "
                    "       ubicacion = %s, "
                    "       id_usuario = %s, "
                    "       comentarios = %s "
                    "WHERE diagnostico.ingresos.id = %s;")

        # asignamos los parametros
        parametros = (self.IdItemIngreso,
                      self.ImporteIngreso,
                      self.IDLABORATORIO,
                      self.IdFinanciamiento,
                      self.RemitoIngreso,
                      self.CantidadIngreso,
                      self.VencimientoIngreso,
                      self.Lote,
                      self.Ubicacion,
                      self.ID,
                      self.ComentariosIngreso,
                      self.IdIngreso)

        # ejecutamos la consulta
        self.Link.ejecutar(consulta, parametros)

    def borraIngreso(self, idingreso):
        """Método que recibe como parámetro la id de un ingreso, y ejecuta
           la consulta de eliminación y actualiza el inventario y también
           la auditoría de ingresos """

        # componemos la consulta de eliminación
        consulta = ("DELETE FROM diagnostico.ingresos "
                    "WHERE diagnostico.ingresos.id = %s;")

        # asignamos los parámetros y ejecutamos
        parametros = (idingreso, )
        self.Link.ejecutar(consulta, parametros)
