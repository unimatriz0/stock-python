#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

    Nombre: form_ingresos.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 26/03/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnostico
    Comentarios: Método que arma el formulario de ingresos de
                 materiales al depósito

"""

# importamos las librerías
from PyQt5 import QtCore, QtGui, QtWidgets
import time
from deposito.ingresos.ingresos import Ingresos
from financiamiento.financiamiento import Financiamiento
from deposito.articulos.articulos import Articulos


class FormIngresos(QtWidgets.QDialog):
    ' Definición de la clase '

    def __init__(self, parent = None):
        """ Definimos el constructor """

        # llama el constructor del padre
        super(FormIngresos, self).__init__(parent)

        # definimos las constantes y las leemos del
        # formulario padre
        padre = parent
        self.IDLABORATORIO = padre.IDLABORATORIO   # clave del laboratorio
        self.USUARIO = padre.USUARIO               # nombre del usuario
        self.IDUSUARIO = padre.IDUSUARIO           # clave del usuario
        self.IdArticulo = 0                        # clave del artículo
        self.IdFinanciamiento = 0                  # clave del financiamiento

        # instanciamos las clases de la base
        self.Ingresos = Ingresos()
        self.Financiamiento = Financiamiento()
        self.Articulos = Articulos()

        # asignamos el laboratorio en las clases hija
        self.Financiamiento.IdLaboratorio = self.IDLABORATORIO
        self.Ingresos.IDLABORATORIO = self.IDLABORATORIO
        self.Articulos.IDLABORATORIO = self.IDLABORATORIO

        # configuramos el formulario
        self.setupUi()

    def setupUi(self):
        ' Configuramos la interfaz '

        # fijamos el tamaño y la posición
        self.setWindowTitle("Ingresos al Depósito")
        self.setGeometry(400, 100, 805, 605)
        self.setWindowIcon(QtGui.QIcon('recursos/logo_fatala.jpg'))

        # establecemos la ventana como modal
        self.setModal(True)

        # definimos la fuente de los labels
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)

        # el título
        lTitulo = QtWidgets.QLabel("Ingresos de materiales al depósito", self)
        lTitulo.setGeometry(QtCore.QRect(10, 10, 580, 26))
        lTitulo.setFont(font)

        # presenta la id del registro
        lId = QtWidgets.QLabel("ID:", self)
        lId.setGeometry(QtCore.QRect(10, 45, 30, 26))
        lId.setFont(font)
        self.tId = QtWidgets.QLineEdit(self)
        self.tId.setGeometry(QtCore.QRect(35, 45, 70, 26))
        self.tId.setToolTip(("Clave del ingreso"))
        self.tId.setReadOnly(True)

        # presenta la descripción del artículo
        lArticulo = QtWidgets.QLabel("Artículo:", self)
        lArticulo.setGeometry(QtCore.QRect(120, 45, 70, 26))
        lArticulo.setFont(font)
        self.cArticulo = QtWidgets.QComboBox(self)
        self.cArticulo.setGeometry(QtCore.QRect(185, 45, 420, 26))
        self.cArticulo.setToolTip(("Seleccione el artículo"))
        self.cArticulo.setEditable(True)
        self.cArticulo.completer()

        # cargamos los artículos
        self.cargaArticulos()

        # presenta el código sop
        lSop = QtWidgets.QLabel("Sop:", self)
        lSop.setGeometry(QtCore.QRect(620, 45, 60, 26))
        lSop.setFont(font)
        self.tSop = QtWidgets.QLineEdit(self)
        self.tSop.setGeometry(QtCore.QRect(660, 45, 100, 26))
        self.tSop.setToolTip("Clave SOP del artículo")

        # pide el remito de entrada
        lRemito = QtWidgets.QLabel("Remito:", self)
        lRemito.setGeometry(QtCore.QRect(10, 80, 60, 26))
        lRemito.setFont(font)
        self.tRemito = QtWidgets.QLineEdit(self)
        self.tRemito.setGeometry(QtCore.QRect(70, 80, 115, 26))
        self.tRemito.setToolTip("Remito de ingreso")

        # pide la cantidad
        lCantidad = QtWidgets.QLabel("Cantidad: ", self)
        lCantidad.setGeometry(QtCore.QRect(190, 80, 90, 26))
        lCantidad.setFont(font)
        self.sCantidad = QtWidgets.QSpinBox(self)
        self.sCantidad.setGeometry(QtCore.QRect(265, 80, 50, 26))
        self.sCantidad.setToolTip("Número de artículos ingresados")

        # pide el importe de la factura
        lImporte = QtWidgets.QLabel("Importe:", self)
        lImporte.setGeometry(QtCore.QRect(325, 80, 60, 26))
        lImporte.setFont(font)
        self.sImporte = QtWidgets.QDoubleSpinBox(self)
        self.sImporte.setGeometry(QtCore.QRect(390, 80, 90, 26))
        self.sImporte.setToolTip("Importe de la factura")

        # pide la fuente de financiamiento
        lFinanciamiento = QtWidgets.QLabel("Financiamiento:", self)
        lFinanciamiento.setGeometry(QtCore.QRect(490, 80, 120, 26))
        lFinanciamiento.setFont(font)
        self.cFinanciamiento = QtWidgets.QComboBox(self)
        self.cFinanciamiento.setGeometry(QtCore.QRect(610, 80, 160, 26))
        self.cFinanciamiento.setToolTip(("Fuente de financiamiento de la compra"))
        self.cFinanciamiento.setEditable(True)
        self.cFinanciamiento.completer()

        # cargamos las fuentes de financiamiento
        self.cargaFinanciamiento()

        # pide la fecha de vencimiento
        lVencimiento = QtWidgets.QLabel("Vencimiento:", self)
        lVencimiento.setGeometry(QtCore.QRect(10, 115, 100, 26))
        lVencimiento.setFont(font)
        self.dVencimiento = QtWidgets.QDateEdit(self)
        self.dVencimiento.setGeometry(QtCore.QRect(110, 115, 110, 26))
        self.dVencimiento.setToolTip("Fecha de vencimiento del artículo")
        self.dVencimiento.setCalendarPopup(True)
        self.dVencimiento.setDisplayFormat("dd/MM/yyyy")
        self.dVencimiento.setDateTime(QtCore.QDateTime.currentDateTime())

        # pide el número de lote
        lLote = QtWidgets.QLabel("Lote:", self)
        lLote.setGeometry(QtCore.QRect(230, 115, 55, 26))
        lLote.setFont(font)
        self.tLote = QtWidgets.QLineEdit(self)
        self.tLote.setGeometry(QtCore.QRect(275, 115, 105, 26))
        self.tLote.setToolTip("Número de lote del artículo")

        # pide la ubicación
        lUbicacion = QtWidgets.QLabel("Ubicación:", self)
        lUbicacion.setGeometry(QtCore.QRect(390, 115, 90, 26))
        lUbicacion.setFont(font)
        self.tUbicacion = QtWidgets.QLineEdit(self)
        self.tUbicacion.setGeometry(QtCore.QRect(470, 115, 300, 26))
        self.tUbicacion.setToolTip("Indique donde será almacenado")

        # presenta el usuario
        lUsuario = QtWidgets.QLabel("Usuario:", self)
        lUsuario.setGeometry(QtCore.QRect(620, 150, 60, 26))
        lUsuario.setFont(font)
        self.tUsuario = QtWidgets.QLineEdit(self)
        self.tUsuario.setGeometry(QtCore.QRect(685, 150, 90, 26))
        self.tUsuario.setToolTip("Usuario que ingresó el artículo")
        self.tUsuario.setReadOnly(True)

        # presenta la fecha de alta
        lAlta = QtWidgets.QLabel("Alta:", self)
        lAlta.setGeometry(QtCore.QRect(645, 180, 60, 26))
        lAlta.setFont(font)
        self.tAlta = QtWidgets.QLineEdit(self)
        self.tAlta.setGeometry(QtCore.QRect(685, 180, 90, 26))
        self.tAlta.setToolTip("Fecha de ingreso del artículo")
        self.tAlta.setReadOnly(True)

        # presenta los comentarios
        lComentarios = QtWidgets.QLabel("Comentarios: ", self)
        lComentarios.setGeometry(QtCore.QRect(10, 140, 120, 26))
        lComentarios.setFont(font)
        self.tComentarios = QtWidgets.QTextEdit(self)
        self.tComentarios.setGeometry(QtCore.QRect(10, 170, 590, 100))
        self.tComentarios.setToolTip("Observaciones del usuario")

        # el botón grabar
        btnGrabar = QtWidgets.QPushButton("Grabar", self)
        btnGrabar.setGeometry(QtCore.QRect(650, 210, 90, 26))
        btnGrabar.setToolTip("Graba el registro en la base")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("recursos/confirmation.png"))
        btnGrabar.setIcon(icon1)
        btnGrabar.clicked.connect(self.validaIngreso)

        # el botón cancelar
        btnCancelar = QtWidgets.QPushButton("Cancelar", self)
        btnCancelar.setGeometry(QtCore.QRect(650, 240, 90, 26))
        btnCancelar.setToolTip("Limpia el formulario de datos")
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("recursos/Borrar1.png"))
        btnCancelar.setIcon(icon)
        btnCancelar.clicked.connect(self.limpiaFormulario)

        # la tabla con los ingresos
        self.tIngresos = QtWidgets.QTableWidget(self)
        self.tIngresos.setGeometry(QtCore.QRect(10, 280, 780, 310))
        self.tIngresos.setColumnCount(10)
        self.tIngresos.setRowCount(0)

        # definimos los títulos de columna
        self.tIngresos.setHorizontalHeaderLabels(('ID',
                                                  'Articulo',
                                                  'Remito',
                                                  'SOP',
                                                  'Cant.',
                                                  'Usuario',
                                                  'Venc.',
                                                  'Ed.',
                                                  'Imp.',
                                                  'El.'))

        # fijamos la alineación de las columnas
        self.tIngresos.horizontalHeaderItem(0).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tIngresos.horizontalHeaderItem(1).setTextAlignment(QtCore.Qt.AlignLeft)
        self.tIngresos.horizontalHeaderItem(2).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tIngresos.horizontalHeaderItem(3).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tIngresos.horizontalHeaderItem(4).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tIngresos.horizontalHeaderItem(5).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tIngresos.horizontalHeaderItem(6).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tIngresos.horizontalHeaderItem(7).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tIngresos.horizontalHeaderItem(8).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tIngresos.horizontalHeaderItem(9).setTextAlignment(QtCore.Qt.AlignCenter)

        # fijmos el tamaño de las columnas
        self.tIngresos.setColumnWidth(0, 50)
        self.tIngresos.setColumnWidth(1, 270)
        self.tIngresos.setColumnWidth(2, 70)
        self.tIngresos.setColumnWidth(3, 70)
        self.tIngresos.setColumnWidth(4, 70)
        self.tIngresos.setColumnWidth(5, 70)
        self.tIngresos.setColumnWidth(6, 70)
        self.tIngresos.setColumnWidth(7, 30)
        self.tIngresos.setColumnWidth(8, 30)
        self.tIngresos.setColumnWidth(9, 30)

        # permitimos reordenar
        self.tIngresos.setSortingEnabled(True)

        # cargamos los ingresos
        self.cargaIngresos()

        # fijamos el evento click
        self.tIngresos.cellClicked.connect(self.accionIngreso)

    def cargaFinanciamiento(self):
        """ Método que carga en el combo las fuentes de financiamiento """

        # obtenemos las fuentes de financiamiento
        nomina = self.Financiamiento.nominaFinanciamiento()

        # agregamos el primer elemento en blanco
        self.cFinanciamiento.addItem("")

        # recorremos el vector
        for registro in nomina:

            # agregamos el elemento
            self.cFinanciamiento.addItem(registro["fuente"])

    def cargaIngresos(self):
        """ Método que a partir de la variable de clase carga
            la grilla con los ingresos al depósito """

        # obtenemos la nómina de ingresos
        nomina = self.Ingresos.listaIngresos()

        # recorremos el vector
        for registro in nomina:

            # insertamos una fila
            fila = self.tIngresos.rowCount()
            self.tIngresos.insertRow(fila)

            # fijamos los valores convirtiendo las cadenas
            self.tIngresos.setItem(fila, 0, QtWidgets.QTableWidgetItem(str(registro["idingreso"])))
            self.tIngresos.setItem(fila, 1, QtWidgets.QTableWidgetItem(registro["descripcion"]))
            self.tIngresos.setItem(fila, 2, QtWidgets.QTableWidgetItem(registro["remito"]))
            self.tIngresos.setItem(fila, 3, QtWidgets.QTableWidgetItem(registro["codigosop"]))
            self.tIngresos.setItem(fila, 4, QtWidgets.QTableWidgetItem(str(registro["cantidad"])))
            self.tIngresos.setItem(fila, 5, QtWidgets.QTableWidgetItem(registro["recibio"]))
            self.tIngresos.setItem(fila, 6, QtWidgets.QTableWidgetItem(registro["vencimiento"]))

            # definimos el ícono de edición
            icon = QtGui.QIcon(QtGui.QPixmap("recursos/13.png"))
            item = QtWidgets.QTableWidgetItem(icon, "")
            self.tIngresos.setItem(fila, 7, item)

            # definimos el ícono de impresión
            icon = QtGui.QIcon(QtGui.QPixmap("recursos/impresora1.png"))
            item = QtWidgets.QTableWidgetItem(icon, "")
            self.tIngresos.setItem(fila, 8, item)

            # define el ícono de borrar
            icon = QtGui.QIcon(QtGui.QPixmap("recursos/90.png"))
            item = QtWidgets.QTableWidgetItem(icon, "")
            self.tIngresos.setItem(fila, 9, item)

    def cargaArticulos(self):
        """ Método que completa el combo con la nómina de artículos """

        # obtenemos la nómina
        nomina = self.Articulos.nominaItems()

        # agregamos el primer elemento en blanco
        self.cArticulo.addItem("")

        # recorremos el vector
        for registro in nomina:

            # agregamos el elemento
            self.cArticulo.addItem(registro["descripcion"])

    def validaIngreso(self):
        """ Método llamado al pulsar el botón grabar que
            valida los datos del formulario """

        # verifica se halla seleccionado un item
        if str(self.cArticulo.currentText()) == "":

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = ("Debe seleccionar el artículo\n"
                       "de la lista")
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # si seleccionó
        else:

            # obtenemos la clave del artículo
            self.IdArticulo = self.Articulos.getClaveItem(str(self.cArticulo.currentText()))


        # verifica se halla ingresado un remito
        if self.tRemito.text() == "":

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = ("Ingrese el número de remito")
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # verifica el importe
        if self.sImporte.value == 0:

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = ("Ingrese el importe de la factura")
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # verifica la fuente de financiamiento
        if str(self.cFinanciamiento.currentText()) == "":

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = ("Seleccione de la lista la\n"
                       "fuente de financiamiento")
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # si seleccionó
        else:

            # obtenemos la clave
            self.IdFinanciamiento = self.Financiamiento.getClaveFinanciamiento(str(self.cFinanciamiento.currentText()))

        # la fecha siempre tiene algo porque la
        # inicializamos a la fecha actual

        # verifica el número de lote
        if self.tLote.text() == "":

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = ("Indique el número de lote")
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # verifica la ubicación
        if self.tUbicacion.text() == "":

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = ("Indique donde será almacenado\n"
                       "el artículo")
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # si llegó hasta aquí grabamos
        self.grabaIngreso()

    def grabaIngreso(self):
        """ Método llamado luego de validar el formulario que
            ejecuta la consulta en la base de datos """

        # según si está insertando o editando
        if self.tId.text() != "":
            self.Ingresos.IdIngreso = self.tId.text()
        else:
            self.Ingresos.IdIngreso = 0

        # almacenamos el resto de los valores
        self.Ingresos.ID = self.IDUSUARIO
        self.Ingresos.IDLABORATORIO = self.IDLABORATORIO
        self.Ingresos.IdItemIngreso = self.IdArticulo
        self.Ingresos.ImporteIngreso = self.sImporte.value()
        self.Ingresos.IdFinanciamiento = self.IdFinanciamiento
        self.Ingresos.RemitoIngreso = self.tRemito.text()
        self.Ingresos.CantidadIngreso = self.sCantidad.value()
        self.Ingresos.Lote = self.tLote.text()
        self.Ingresos.Ubicacion = self.tUbicacion.text()
        self.Ingresos.ComentariosIngreso = self.tComentarios.text()

        # convertimos la fecha de vencimiento
        fechatemp = self.dVencimiento.date().toPyDate()
        self.Ingresos.VencimientoIngreso = fechatemp.strftime("%d/%m/%Y")

        # grabamos el registro
        self.Ingresos.grabaIngreso()

        # limpiamos el formulario
        self.limpiaFormulario()

        # recargamos la grilla
        self.cargaIngresos()

    def borraIngreso(self, idingreso):
        """ Método llamado al pulsar el botón borrar que luego
            de pedir confirmación ejecuta la consulta de
            eliminación

            Parámetros: idingreso - clave del registro """

        # pide confirmación
        titulo = "Confirmación"
        mensaje = "Está seguro que desea\n"
        mensaje += "eliminar el registro?"
        respuesta = QtWidgets.QMessageBox.question(self,
                                         titulo,
                                         mensaje,
                                         QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)

        # si confirmó
        if respuesta == QtWidgets.QMessageBox.Yes:

            # elimina el registro
            self.Ingresos.borraIngreso(idingreso)

            # limpia y recarga la grilla
            self.cargaIngresos()

    def verIngreso(self, idingreso):
        """ Método que carga los datos de un ingreso en el
            formulario

            Parámetros: idingreso - entero clave del registro """

        # obtenemos los datos del registro
        self.Ingresos.getDatosIngreso(idingreso)

        # presentamos el registro

    def remitoIngreso(self, idingreso):
        """ Método que genera el remito con los datos de
            un ingreso

            Parámetros: idingreso - clave del registro """

    def limpiaFormulario(self):
        """ Método llamado al pulsar el botón cancelar o
            luego de grabar un ingreso que limpia el
            formulario de datos """

        # inicializamos los valores
        self.tId.setText("")
        self.cArticulo.setCurrentIndex(0)
        self.IdArticulo = 0
        self.tSop.setText("")
        self.tRemito.setText("")
        self.sCantidad.setValue(0)
        self.sImporte.setValue(0)
        self.cFinanciamiento.setCurrentIndex(0)
        self.IdFinanciamiento = 0
        self.dVencimiento.setDateTime(QtCore.QDateTime.currentDateTime())
        self.tLote.setText("")
        self.tUbicacion.setText("")
        self.tComentarios.setText("")
        self.tUsuario.setText(self.USUARIO)
        self.tAlta.setText(time.strftime("%d/%m/%Y"))

    def accionIngreso(self, fila, columna):
        """ Método llamado al pulsar un ícono en la grilla,
            recibe como parámetro la fila y columna
            pulsada y obtiene la clave del ingreso y
            llama el método correspondiente """

        # obtenemos el valor de la celda
        idingreso = self.tIngreso.item(fila, 0).text()

        # si pulsó sobre editar
        if columna == 7:

            # presentamos el registro
            self.verIngreso(idingreso)

        # si pulsó imprimir
        elif columna == 8:

            # imprimimos la etiqueta
            self.remitoIngreso(idingreso)

        # si pulsó borrar
        elif columna == 9:

            # elimina el registro
            self.borraIngreso(idingreso)
