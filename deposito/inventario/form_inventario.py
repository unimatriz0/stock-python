#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Nombre: form_inventario.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 24/03/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnostico
    Comentarios: Clase que define el formulario con la grilla
                 del inventario existente

"""

# importamos las librerías
from PyQt5 import QtCore, QtGui, QtWidgets
from deposito.inventario.inventario import Inventario

class FormInventario(QtWidgets.QDialog):
    """ Definición de la clase """

    def __init__(self, parent = None):
        """ Constructor de la clase """

        # inicializamos la variable del laboratorio
        self.IDLABORATORIO = 0

        # llama al constructor del padre
        super(FormInventario, self).__init__(parent)

        # construimos la interfaz
        self.setupUi()

    def setupUi(self):
        """ Define la interfaz """

        # fijamos las propiedades
        # dimensionamos y posicionamos el diálogo
        self.setWindowTitle("Inventario en Depósito")
        self.setGeometry(450, 150, 590, 453)
        self.setWindowIcon(QtGui.QIcon('recursos/logo_fatala.jpg'))

        # establecemos la ventana como modal
        self.setModal(True)

        # presentamos el título
        self.lTitulo = QtWidgets.QLabel("Control de Inventario en Depósito", self)
        self.lTitulo.setGeometry(QtCore.QRect(10, 5, 370, 26))

        # presenta la grilla de existentes
        self.tInventario = QtWidgets.QTableWidget(self)
        self.tInventario.setGeometry(QtCore.QRect(10, 45, 580, 400))
        self.tInventario.setColumnCount(6)
        self.tInventario.setRowCount(0)

        # define los títulos
        self.tInventario.setHorizontalHeaderLabels(('Descripción',
                                                   'Sop',
                                                   'Critico',
                                                   'Existentes',
                                                   'Imp.',
                                                   'Imp.Total'))

        # define el ancho de las columnas
        self.tInventario.setColumnWidth(0, 300)
        self.tInventario.setColumnWidth(1, 50)
        self.tInventario.setColumnWidth(2, 50)
        self.tInventario.setColumnWidth(3, 50)
        self.tInventario.setColumnWidth(4, 50)
        self.tInventario.setColumnWidth(5, 50)

        # define la alineación de las columnas
        self.tInventario.horizontalHeaderItem(0).setTextAlignment(QtCore.Qt.AlignLeft)
        self.tInventario.horizontalHeaderItem(1).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tInventario.horizontalHeaderItem(2).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tInventario.horizontalHeaderItem(3).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tInventario.horizontalHeaderItem(4).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tInventario.horizontalHeaderItem(5).setTextAlignment(QtCore.Qt.AlignCenter)

        # permitimos reordenar
        self.tInventario.setSortingEnabled(True)

    def cargaInventario(self):
        """ Método que a partir de las variables de clase
            carga la grilla con el stock del laboratorio
            activo """

        # obtenemos el inventario
        Deposito = Inventario()
        Deposito.IDLABORATORIO = self.IDLABORATORIO
        articulos = Deposito.listaInventario()

        # recorremos el vector
        for registro in articulos:

            # insertamos una fila
            fila = self.tInventario.rowCount()
            self.tInventario.insertRow(fila)

            # fijamos los valores convirtiendo las cadenas
            self.tInventario.setItem(fila, 0, QtWidgets.QTableWidgetItem(registro["descripcion"]))
            self.tInventario.setItem(fila, 1, QtWidgets.QTableWidgetItem(registro["codigosop"]))
            self.tInventario.setItem(fila, 2, QtWidgets.QTableWidgetItem(str(registro["critico"])))
            self.tInventario.setItem(fila, 3, QtWidgets.QTableWidgetItem(str(registro["existencia"])))
            self.tInventario.setItem(fila, 4, QtWidgets.QTableWidgetItem(str(registro["importe"])))

            # calculamos el total y lo agregamos
            total = registro["existencia"] * registro["importe"]
            self.tInventario.setItem(fila, 5, QtWidgets.QTableWidgetItem(str(total)))
