#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Nombre: inventario.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 24/03/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnostico
    Comentarios: Clase que contiene los métodos de consulta del 
                 inventario en existencia

"""

# importamos la librería
from clases.dbapi import Conexion


class Inventario:
    'Definición de la clase'

    def __init__(self):
        ' constructor de la clase '

        # inicia la clave del laboratorio
        self.IDLABORATORIO = 0
        
        # instanciamos la conexión
        self.Link = Conexion()

    def listaInventario(self):
        """Método que lista el estado del inventario """

        # componemos la consulta con la existencia en depòsito
        # para el laboratorio del usuario
        consulta = ("SELECT diagnostico.v_inventario.iditem AS iditem, "
                    "       diagnostico.v_inventario.descripcion AS descripcion, "
                    "       diagnostico.v_inventario.codigosop AS codigosop, "
                    "       diagnostico.v_inventario.critico AS critico, "
                    "       diagnostico.v_inventario.existencia AS existencia, "
                    "       diagnostico.v_inventario.importe AS importe, "
                    "       diagnostico.v_inventario.laboratorio AS laboratorio "
                    "FROM diagnostico.v_inventario "
                    "WHERE diagnostico.v_inventario.idlaboratorio = %s "
                    "ORDER BY diagnostico.v_inventario.descripcion; ")

        # asignamos los paràmetros
        parametros = (self.IDLABORATORIO,)

        # ejecutamos la consulta y retornamos el vector
        resultado = self.Link.consultar(consulta, parametros)
        return resultado
