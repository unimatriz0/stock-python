#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

    Nombre: form_articulos.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 09/08/2017
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Comentarios: Clase que arma el formulario para la edición y alta de los
                 items (diccionario) del depósito

"""

# importamos las librerías
# para traducir los formularios de ui a py usamos
# pyuic5 input.ui -o output.py

# importamos las librerías
from PyQt5 import QtWidgets, QtGui, QtCore
import time

# importamos las clases
from financiamiento.financiamiento import Financiamiento
from deposito.articulos.articulos import Articulos
from herramientas.qlabelclickeable import QLabelClickeable


class formArticulos(QtWidgets.QDialog):
    'Definición de la clase'

    def __init__(self, parent=None):
        'Definimos el constructor de la clase'

        # definimos las constantes del usuario
        self.IDUSUARIO = 0
        self.USUARIO = ""
        self.IDLABORATORIO = 0

        # llama al constructor del padre
        super(formArticulos, self).__init__(parent)

        self.setupUi()

    def setupUi(self):
        """ Método que configura el formulario, lo llamamos manualmente
            para poder asignar primero las variables de clase """

        # instanciamos la clase
        self.items = Articulos()

        # asignamos en la clase los valores
        self.items.ID = self.IDUSUARIO                  # clave del usuario
        self.items.USUARIO = self.USUARIO               # nombre del usuario
        self.items.IDLABORATORIO = self.IDLABORATORIO   # clave del laboratorio

        # dimensionamos y posicionamos el diálogo
        self.setWindowTitle("Edición de Artículos")
        self.setGeometry(400, 120, 660, 550)
        self.setWindowIcon(QtGui.QIcon('recursos/logo_fatala.jpg'))

        # establecemos la ventana como modal
        self.setModal(True)

        # definimos la imagen
        self.Foto = ""

        # definimos la fuente de los labels
        font = QtGui.QFont()
        font.setBold(True)
        font.setWeight(75)

        # presentamos el título
        lTitulo = QtWidgets.QLabel("Diccionario de Elementos del Deposito", self)
        lTitulo.setGeometry(10, 20, 601, 30)
        lTitulo.setFont(font)

        # presenta la id del registro
        lId = QtWidgets.QLabel("ID:", self)
        lId.setGeometry(10, 50, 20, 24)
        lId.setFont(font)
        self.tId = QtWidgets.QLineEdit(self)
        self.tId.setGeometry(36, 50, 70, 24)
        self.tId.setToolTip("Clave del Registro")
        self.tId.setReadOnly(True)

        # presenta la descripción
        lDescripcion = QtWidgets.QLabel('Descripción:', self)
        lDescripcion.setGeometry(116, 48, 90, 24)
        lDescripcion.setFont(font)
        self.tDescripcion = QtWidgets.QLineEdit(self)
        self.tDescripcion.setGeometry(210, 48, 420, 24)
        self.tDescripcion.setToolTip("Ingrese la Descripción del Item")

        # presenta la foto del item por defecto sin imagen
        self.gImagen = QLabelClickeable(self)
        self.gImagen.setGeometry(10, 83, 110, 110)
        self.gImagen.setToolTip("Pulse para cargar una imagen del item")
        self.gImagen.setCursor(QtCore.Qt.PointingHandCursor)
        self.gImagen.setStyleSheet("QLabel {background-color: white; border: 1px solid"
                                   "#01DFD7; border-radius: 2px;}")

        # conectamos el evento
        self.gImagen.clicked.connect(self.cargaImagen)

        # pide el nivel crítico
        lCritico = QtWidgets.QLabel('Crítico:', self)
        lCritico.setGeometry(140, 89, 50, 24)
        lCritico.setFont(font)
        self.sCritico = QtWidgets.QSpinBox(self)
        self.sCritico.setGeometry(195, 89, 60, 24)
        self.sCritico.setToolTip("Indica la cantidad crítica del item")

        # presenta el valor
        lValor = QtWidgets.QLabel("Valor:", self)
        lValor.setGeometry(275, 89, 40, 24)
        lValor.setFont(font)
        self.tValor = QtWidgets.QLineEdit(self)
        self.tValor.setGeometry(320, 89, 113, 24)
        self.tValor.setToolTip("Indique el costo unitario")

        # presenta el código sop
        lSop = QtWidgets.QLabel("Sop:", self)
        lSop.setGeometry(440, 89, 40, 24)
        lSop.setFont(font)
        self.tSop = QtWidgets.QLineEdit(self)
        self.tSop.setGeometry(475, 89, 160, 24)
        self.tSop.setToolTip("Clave Sop del artículo")

        # presenta el usuario
        lUsuario = QtWidgets.QLabel("Usuario:", self)
        lUsuario.setGeometry(140, 130, 70, 24)
        lUsuario.setFont(font)
        self.tUsuario = QtWidgets.QLineEdit(self.USUARIO, self)
        self.tUsuario.setGeometry(200, 130, 70, 24)
        self.tUsuario.setToolTip("Usuario que ingresó el item")
        self.tUsuario.setReadOnly(True)

        # presenta la fecha de alta
        lFechaAlta = QtWidgets.QLabel("Fecha Alta: ", self)
        lFechaAlta.setGeometry(280, 130, 80, 24)
        lFechaAlta.setFont(font)
        self.tFechaAlta = QtWidgets.QLineEdit(self)
        self.tFechaAlta.setGeometry(365, 130, 90, 24)
        self.tFechaAlta.setToolTip("Fecha de alta del registro")
        self.tFechaAlta.setReadOnly(True)

        # fijamos el valor por defecto
        self.tFechaAlta.setText(time.strftime("%d/%m/%Y"))

        # presenta el botón grabar
        btnGrabar = QtWidgets.QPushButton("Grabar", self)
        btnGrabar.setGeometry(465, 125, 90, 34)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("recursos/confirmation.png"))
        btnGrabar.setIcon(icon1)
        btnGrabar.setToolTip("Pulse para grabar el registro")
        btnGrabar.clicked.connect(self.grabaItem)

        # presenta el botón cancelar
        btnCancelar = QtWidgets.QPushButton("Cancelar", self)
        btnCancelar.setGeometry(565, 125, 90, 34)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("recursos/Borrar1.png"))
        btnCancelar.setIcon(icon)
        btnCancelar.setToolTip("Pulse para cerrar sin guardar")
        btnCancelar.clicked.connect(self.cancelar)

        # agregamos la tabla
        self.tArticulos = QtWidgets.QTableWidget(self)
        self.tArticulos.setGeometry(10, 200, 640, 340)
        self.tArticulos.setRowCount(0)
        self.tArticulos.setColumnCount(7)
        self.tArticulos.setHorizontalHeaderLabels(('ID',
                                                   'Articulo',
                                                   'Valor',
                                                   'Critico',
                                                   'Ed.',
                                                   'Imp.',
                                                   'El.'))

        # fijamos la alineación de las columnas
        self.tArticulos.horizontalHeaderItem(0).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tArticulos.horizontalHeaderItem(1).setTextAlignment(QtCore.Qt.AlignLeft)
        self.tArticulos.horizontalHeaderItem(2).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tArticulos.horizontalHeaderItem(3).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tArticulos.horizontalHeaderItem(4).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tArticulos.horizontalHeaderItem(5).setTextAlignment(QtCore.Qt.AlignCenter)
        self.tArticulos.horizontalHeaderItem(6).setTextAlignment(QtCore.Qt.AlignCenter)

        # fijmos el tamaño de las columnas
        self.tArticulos.setColumnWidth(0, 30)
        self.tArticulos.setColumnWidth(1, 300)
        self.tArticulos.setColumnWidth(2, 100)
        self.tArticulos.setColumnWidth(3, 100)
        self.tArticulos.setColumnWidth(4, 30)
        self.tArticulos.setColumnWidth(5, 30)
        self.tArticulos.setColumnWidth(6, 30)

        # permitimos reordenar
        self.tArticulos.setSortingEnabled(True)

        # agregamos los artículos a la tabla
        self.cargaArticulos()

        # define los eventos de la grilla
        self.tArticulos.cellClicked.connect(self.accionArticulo)

        # mostramos el formulario
        self.show()

    def muestraItem(self, iditem):
        """Método que recibe como parámetro la id de un item, ejecuta
           la consulta y carga en el formulario los datos del registro """

        # ejecutamos la consulta
        self.items.getItem(iditem)

        # ahora asignamos en el formulario
        self.tId.setText(str(self.items.IdItem))
        self.tDescripcion.setText(self.items.Descripcion)
        self.tValor.setText(str(self.items.Valor))
        self.tSop.setText(self.items.CodigoSop)
        self.sCritico.setValue(self.items.Critico)
        self.tFechaAlta.setText(self.items.Fecha)
        self.tUsuario.setText(self.items.Usuario)

        # si existe una imagen
        if self.items.Imagen:

            # cargamos la imagen en un pixmap
            foto = QtGui.QPixmap()
            foto.loadFromData(self.items.Imagen, "PNG", QtCore.Qt.AutoColor)

        # si no hay imagen
        else:

            # fijamos la imagen por defecto
            foto = QtGui.QPixmap("recursos/sin_imagen.jpg")

        # redimensionamos la immagen y mostramos
        self.gImagen.setPixmap(foto)

        # establecemos el foco
        self.tDescripcion.setFocus()

    def nuevoItem(self):
        """Metodo que simplemente carga en los campos de usuario y
           fecha de alta los datos de la sesion """

        # fijamos los valores
        self.tId.setText("")
        self.tSop.setText("")
        self.tDescripcion.setText("")
        self.sCritico.setValue(0)
        self.tValor.setText("")
        self.tUsuario.setText(self.USUARIO)
        self.tFechaAlta.setText(time.strftime("%d/%m/%Y"))

        # fijamos la imagen por defecto e inicializamos
        # la cadena de la imagen
        self.gImagen.clear()

    def grabaItem(self):
        """Método que verifica el formulario y luego instancia la clase
           para grabar el registro en la base """

        # asignamos en las constantes
        self.items.ID = self.IDUSUARIO                  # clave del usuario
        self.items.USUARIO = self.USUARIO               # nombre del usuario
        self.items.IDLABORATORIO = self.IDLABORATORIO   # clave del laboratorio

        # si no ingresó la descripción
        if str(self.tDescripcion.text()) == "":

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = ("Debe ingresar la descripción\n"
                       "del item")
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # el costo permite dejarlo en blanco

        # valor crítico permite ponerlo a cero como
        # indicador de no informar alertas

        # asignamos los valores en la clase y grabamos

        # si está editando de otra forma nos aseguramos
        # de inicializar la clave
        if self.tId.text() != "":
            self.items.tId = self.tId.text()
        else:
            self.items.tId = 0
        self.items.Descripcion = self.tDescripcion.text()
        self.items.Valor = self.tValor.text()
        self.items.Critico = self.sCritico.value()
        self.items.CodigoSop = self.tSop.text()

        # asignamos la ruta de la imagen
        self.items.Imagen = self.Foto

        # ejecutamos la consulta
        self.items.grabaItem()

        # limpiamos el formulario
        self.nuevoItem()

        # recargamos la grilla del formulario
        self.cargaArticulos()

    def cargaArticulos(self):
        ' Método que carga la grilla de artículos '

        # limpiamos la grilla
        self.tArticulos.setRowCount(0)

        # obtenemos la nómina de items
        nomina = self.items.nominaItems()

        # recorremos el vector
        for registro in nomina:

            # insertamos una fila
            fila = self.tArticulos.rowCount()
            self.tArticulos.insertRow(fila)

            # fijamos los valores convirtiendo las cadenas
            self.tArticulos.setItem(fila, 0, QtWidgets.QTableWidgetItem(str(registro["iditem"])))
            self.tArticulos.setItem(fila, 1, QtWidgets.QTableWidgetItem(registro["descripcion"]))
            self.tArticulos.setItem(fila, 2, QtWidgets.QTableWidgetItem(str(registro["valor"])))
            self.tArticulos.setItem(fila, 3, QtWidgets.QTableWidgetItem(str(registro["critico"])))

            # definimos el ícono de edición
            icon = QtGui.QIcon(QtGui.QPixmap("recursos/13.png"))
            item = QtWidgets.QTableWidgetItem(icon, "")
            self.tArticulos.setItem(fila, 4, item)

            # definimos el ícono de impresión
            icon = QtGui.QIcon(QtGui.QPixmap("recursos/impresora1.png"))
            item = QtWidgets.QTableWidgetItem(icon, "")
            self.tArticulos.setItem(fila, 5, item)

            # define el ícono de borrar
            icon = QtGui.QIcon(QtGui.QPixmap("recursos/90.png"))
            item = QtWidgets.QTableWidgetItem(icon, "")
            self.tArticulos.setItem(fila, 6, item)

    def cancelar(self):
        'Evento llamado al pulsar el botón cancelar'

        # simplemente cerramos
        self.close()

    def cargaImagen(self):
        """Método llamado al pulsar sobre la imagen, abre el cuadro
           de diálogo y permite seleccionar una imagen del disco """

        # abrimos el cuadro de diálogo (cuidado porque retorna como un
        # array y el primer elemento es el nombre, el segundo es el
        # filtro que agregamos)
        nombre_fichero = QtWidgets.QFileDialog.getOpenFileName(self,
                                                     'Elegir Imagen',
                                                     '',
                                                     'Archivos de imagen (*.png *.jpg)')

        # si seleccionó un archivo
        if nombre_fichero[0]:

            # cargamos la imagen
            imagen = QtGui.QPixmap(nombre_fichero[0]).scaled(110, 110,
                                                      QtCore.Qt.KeepAspectRatio,
                                                      QtCore.Qt.SmoothTransformation)

            # la mostramos en el label
            self.gImagen.setPixmap(imagen)

            # asignamos el nombre del fichero
            self.Foto = nombre_fichero[0]

    def keyPressEvent(self, event):
        'Definición del evento keypress '

        # si pulsó enter
        if event.key() == QtCore.Qt.Key_Return or event.key() == QtCore.Qt.Key_Enter:

            # si el foco lo tiene la descripción
            if self.tDescripcion.hasFocus():

                # setea el foco en el costo
                self.tValor.setFocus()

            # si el foco lo tiene el valor
            elif self.tValor.hasFocus():

                # setea el foco el crítico
                self.sCritico.setFocus()

            # si el foco lo tiene el crítico
            elif self.sCritico.hasFocus():

                # setea el foco en el botón
                self.btnGrabar.setFocus()

    def borraArticulo(self, iditem):
        """ Método que recibe como parámetro la clave de un
            artículo y luego de pedir confirmación y ificar
            que no tenga hijos, ejecuta la consulta """

        # verifica si puede borrar
        if not self.items.puedeBorrar(iditem):

            # presenta el mensaje y retorna
            titulo = 'Atención'
            mensaje = "Ese artículo ya está asignado\n"
            mensaje += "a una compra, no se puede eliminar"
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # pide confirmación
        titulo = "Confirmación"
        mensaje = "Está seguro que desea\n"
        mensaje += "eliminar el registro?"
        respuesta = QtWidgets.QMessageBox.question(self,
                                         titulo,
                                         mensaje,
                                         QtWidgets.QMessageBox.Yes, QtWidgets.QMessageBox.No)

        # si confirmó
        if respuesta == QtWidgets.QMessageBox.Yes:

            # elimina el registro
            self.Fuentes.borraItem(iditem)

            # limpia y recarga la grilla
            self.cargaArticulos()

    def imprimeEtiqueta(self, iditem):
        """ Método que recibe como parámetro la clave de un
            artículo y genera la etiqueta del mismo """

    def accionArticulo(self, fila, columna):
        """ Método llamado al pulsar un ícono en la grilla,
            recibe como parámetro la fila y columna
            pulsada y obtiene la clave del artículo y
            llama el método correspondiente """

        # obtenemos el valor de la celda
        iditem = self.tArticulos.item(fila, 0).text()

        # si pulsó sobre editar
        if columna == 4:

            # presentamos el registro
            self.muestraItem(iditem)

        # si pulsó imprimir
        elif columna == 5:

            # imprimimos la etiqueta
            self.imprimeEtiqueta(iditem)

        # si pulsó borrar
        elif columna == 6:

            # elimina el registro
            self.borraArticulo(iditem)
