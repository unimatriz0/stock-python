#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Nombre: articulos.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 01/02/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnostico
    Comentarios: Clase que contiene los metodos para operar con las tablas de
                 control de stock

"""

# importamos la librería
from clases.dbapi import Conexion
import base64


class Articulos:
    'Definición de la clase'

    def __init__(self):
        ' constructor de la clase '

        # constantes de la clase que identifican al usuario
        self.ID = ""
        self.USUARIO = ""
        self.IDLABORATORIO = ""

        # variables públicas de la clase de la tabla de items
        self.IdItem = 0
        self.Descripcion = ""
        self.Valor = ""
        self.Imagen = ""
        self.Critico = 0
        self.CodigoSop = ""
        self.Fecha = ""
        self.Usuario = ""
        self.IdUsuario = ""

        # instanciamos la conexión
        self.Link = Conexion()

    def nominaItems(self):
        """Método que retorna un resultset con la nómina completa
           de los items del stock """

        # componemos la consulta
        consulta = ("SELECT diagnostico.v_items.id_item AS iditem, "
                    "       diagnostico.v_items.descripcion AS descripcion, "
                    "       diagnostico.v_items.valor AS valor, "
                    "       diagnostico.v_items.critico AS critico, "
                    "       diagnostico.v_items.usuario AS usuario "
                    "FROM diagnostico.v_items "
                    "WHERE diagnostico.v_items.id_laboratorio = '%s' "
                    "ORDER BY diagnostico.v_items.descripcion;")

        # fijamos el parámetro
        parametros = (self.IDLABORATORIO, )

        # obtenemos el vector
        resultado = self.Link.consultar(consulta, parametros)

        # retornamos el vector
        return resultado

    def grabaItem(self):
        """Método que produce la consulta de inserción o eliminación
           según el caso y la ejecuta, retorna la id del registro
           afectado """

        # si está insertando
        if self.IdItem == 0:
            self.nuevoItem()
        else:
            self.editaItem()

        # verificamos si actualiza la imagen
        self.grabaImagen()

        # retornamos la clave
        return self.IdItem

    def nuevoItem(self):
        """ Método que realiza la consulta de inserción de un nuevo item """

        # componemos la consulta
        consulta = ("INSERT INTO diagnostico.items "
                    "            (id_laboratorio, "
                    "             descripcion, "
                    "             valor, "
                    "             codigosop, "
                    "             critico, "
                    "             id_usuario) "
                    "            VALUES "
                    "            (%s, %s, %s, %s, %s, %s);")

        # asignamos los parámetros
        parametros = (self.IDLABORATORIO,
                      self.Descripcion,
                      self.Valor,
                      self.CodigoSop,
                      self.Critico,
                      self.ID)

        # ejecutamos y obtenemos la id
        self.IdItem = self.Link.ejecutar(consulta, parametros)

    def editaItem(self):
        """ Método que ejecuta la consulta de edición de un item """

        # componemos la consulta
        consulta = ("UPDATE diagnostico.items SET "
                    "       descripcion = %s, "
                    "       valor = %s, "
                    "       codigosop = %s, "
                    "       critico = %s, "
                    "       id_usuario = %s "
                    "WHERE id = %s;")

        # asignamos los parámetros
        parametros = (self.Descripcion,
                      self.Valor,
                      self.CodigoSop,
                      self.Critico,
                      self.ID,
                      self.IdItem)

        # ejecutamos la consulta
        self.Link.ejecutar(consulta, parametros)

    def grabaImagen(self):
        """ Método que actualiza la imagen del item luego de
            grabar y estar seguros de tener la clave del registro """

        # si la imagen no es nula
        if self.Imagen != "":

            # leemos el archivo
            with open(self.Imagen, 'rb') as f:
                foto = f.read()

            # la convertimos a base 64
            foto64 = base64.encodestring(foto)

            # actualizamos en la base
            consulta = ("UPDATE diagnostico.items SET "
                        "       imagen = %s "
                        "WHERE diagnostico.items.id = %s;")
            parametros = (foto64, self.IdItem)
            self.Link.ejecutar(consulta, parametros)

    def getItem(self, idItem):
        """Método que recibe como parámetro la id de un item del stock
           y obtiene los datos del registro y los asigna en la
           variable de clase """

        # componemos la consulta
        consulta = ("SELECT diagnostico.v_items.id_item AS iditem, "
                    "       diagnostico.v_items.descripcion AS descripcion, "
                    "       diagnostico.v_items.valor AS valor, "
                    "       diagnostico.v_items.imagen AS imagen, "
                    "       diagnostico.v_items.critico AS critico, "
                    "       diagnostico.v_items.codigosop AS codigosop, "
                    "       diagnostico.v_items.fecha AS fecha_alta, "
                    "       diagnostico.v_items.id_usuario AS id_usuario, "
                    "       diagnostico.v_items.usuario AS usuario "
                    "FROM diagnostico.v_items "
                    "WHERE diagnostico.v_items.id_item = %s;")

        # fijamos los parámetros y ejecutamos la consulta
        parametros = (idItem,)
        resultado = self.Link.obtener(consulta, parametros)

        # asignamos los valores a las variables de clase
        self.IdItem = resultado["iditem"]
        self.Descripcion = resultado["descripcion"]
        self.Valor = resultado["valor"]
        self.Imagen = resultado["imagen"]
        self.CodigoSop = resultado["codigosop"]
        self.Critico = resultado["critico"]
        self.Fecha = resultado["fecha_alta"]
        self.IdUsuario = resultado["id_usuario"]
        self.Usuario = resultado["usuario"]

    def getClaveItem(self, item):
        """ Método que recibe el nombre de un artículo y
            retorna la clave del mismo """

        # componemos la consulta
        consulta = ("SELECT diagnostico.items.id AS id "
                    "FROM diagnostico.items "
                    "WHERE diagnostico.items.descripcion = %s AND "
                    "      diagnostico.items.id_laboratorio = %s; ")

        # asignamos los parámetros y obtenemos el registro
        parametros = (item, self.IDLABORATORIO)
        resultado = self.Link.obtener(consulta, parametros)
        return resultado["id"]

    def verificaItem(self, item):
        """Método que recibe como parámetro el nombre de un item y
           verifica que no se encuentre en la base, retorna true
           si se encuentra, falso en caso contrario """

        # componemos la consulta
        consulta = ("SELECT COUNT(diagnostico.items.id) AS registros "
                    "FROM diagnostico.items "
                    "WHERE diagnostico.items.descripcion = %s;")

        # asignamos los parámetros y ejecutamos la consulta
        parametros = (item, )
        resultado = self.Link.obtener(consulta, parametros)

        # si no hubo registros
        if resultado["registros"] == 0:
            return False
        else:
            return True

    def puedeBorrar(self, iditem):
        """ Método que recibe como parámetro la clave de un item
            y verifica si tiene hijos, llamado antes de eliminar
            el registro """

        # componemos la consulta sobre los ingresos
        consulta = ("SELECT COUNT(diagnostico.ingresos.id) AS ingresos "
                    "FROM diagnostico.ingresos "
                    "WHERE diagnostico.ingresos.item = %s; ")

        # verificamos si tiene ingresos
        parametros = (iditem,)
        resultado = self.Link.consultar(consulta, parametros)
        ingresos = resultado["ingresos"]

        # componemos la consulta sobre los egresos
        consulta = ("SELECT COUNT(diagnostico.egresos.id) AS egresos "
                    "FROM diagnostico.egresos "
                    "WHERE diagnostico.egresos.item = %s; ")

        # verificamos si tiene egresos
        resultado = self.Link.consultar(consulta, parametros)
        egresos = resultado["egresos"]

        # si alguno de los dos tiene registros
        if ingresos != 0 or egresos != 0:
            return False
        else:
            return True

    def borraItem(self, iditem):
        """ Método que recibe como parámetro la clave de un
            artícuco y ejecuta la consulta de eliminación """

        # componemos y ejecutamos la consulta
        consulta = "DELETE FROM diagnostico.items WHERE id = %s; "
        parametros = (iditem, )
        self.Link.ejecutar(consulta, parametros)
