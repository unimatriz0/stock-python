#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Nombre: egresos.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 01/02/2019
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnostico
    Comentarios: Clase que contiene los metodos para operar con las tablas de
                 egresos de materiales del control de stock

"""

# importamos la librería
from clases.dbapi import Conexion


class Egresos:
    ' Definición de la clase '

    def __init__(self):
        ' Constructor de la clase '

        # constantes de la clase que identifican al usuario
        self.ID = ""                # clave del usuario
        self.USUARIO = ""           # nombre del usuario
        self.IDLABORATORIO = ""     # clave del laboratorio

        # variables públicas de la tabla de egresos
        self.IdEgreso = 0              # clave del egreso
        self.ItemEgreso = ""           # nombre del item
        self.IdItemEgreso = 0          # clave del item
        self.CodigoSop = ""            # código sop del item
        self.Imagen = ""               # imagen del item
        self.RemitoEgreso = ""         # número de remito
        self.CantidadEgreso = ""       # cantidad entregada
        self.IdEntregoEgreso = 0       # clave del usuario que entregó
        self.EntregoEgreso = ""        # nombre del usuario
        self.IdRecibioEgreso = 0       # clave del usuario que recibió
        self.RecibioEgreso = ""        # nombre del usuario
        self.FechaEgreso = ""          # fecha de entrega
        self.ComentariosEgreso = ""    # comentarios del usuario

        # instanciamos la conexión
        self.Link = Conexion()

    def listaEgresoFecha(self, inicio, fin):
        """Método que lista las salidas del depósito en un cierto período
           de tiempo, recibe como parámetros las fechas de inicio y fin
           en formato dd/mm/YYYY 
           
           Parámetros: inicio - string con fecha de inicio del reporte 
                       fin - string con la fecha de finalización """

        # componemos la consulta de los egresos solo para el laboratorio
        # del usuario
        consulta = ("SELECT diagnostico.v_egresos.idegreso AS idegreso, "
                    "       diagnostico.v_egresos.descripcion AS descripcion, "
                    "       diagnostico.v_egresos.codigosop AS codigosop, "
                    "       diagnostico.v_egresos.laboratorio AS laboratorio, "
                    "       diagnostico.v_egresos.remito AS remito, "
                    "       diagnostico.v_egresos.cantidad AS cantidad, "
                    "       diagnostico.v_egresos.fecha_egreso AS fecha, "
                    "       diagnostico.v_egresos.nombre_entrego AS entrego, "
                    "       diagnostico.v_egresos.nombre_recibio AS recibio "
                    "FROM diagnostico.v_egresos "
                    "WHERE diagnostico.v_egresos.id_laboratorio = %s AND "
                    "      STR_TO_DATE(%s, '%%d/%%m/%%Y') > STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%%d/%%m/%%Y') AND "
                    "      STR_TO_DATE(%s, '%%d/%%m/%%Y') < STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%%d/%%m/%%Y') "
                    "ORDER BY STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%%d/%%m/%%Y') DESC "
                    "         diagnostico.v_egresos.descripcion;")

        # asignamos los parámetros
        parametros = (self.IDLABORATORIO, inicio, fin)

        # ejecutamos la consulta y retornamos el vector
        resultado = self.Link.consultar(consulta, parametros)
        return resultado

    def listaEgresoItem(self, item):
        """Método que recibe como parámetro la clave de un item y lista
           las salidas del depósito de ese item para el laboratorio del
           usuario """

        consulta = ("SELECT diagnostico.v_egresos.idegreso AS idegreso, "
                    "       diagnostico.v_egresos.descripcion AS descripcion, "
                    "       diagnostico.v_egresos.codigosop AS codigosop, "
                    "       diagnostico.v_egresos.remito AS remito, "
                    "       diagnostico.v_egresos.laboratorio AS laboratorio, "
                    "       diagnostico.v_egresos.cantidad AS cantidad, "
                    "       diagnostico.v_egresos.nombre_entrego AS entrego, "
                    "       diagnostico.v_egresos.nombre_recibio AS recibio, "
                    "       diagnostico.v_egresos.fecha_egreso AS fecha "
                    "FROM diagnostico.v_egresos "
                    "WHERE diagnostico.v_egresos.id_laboratorio = %s AND "
                    "      diagnostico.v_egresos.iditem = %s "
                    "ORDER BY STR_TO_DATE(diagnostico.v_egresos.fecha_egreso, '%%d/%%m/%%Y') DESC "
                    "         diagnostico.v_egresos.descripcion;")

        # asignamos los paràmetros
        parametros = (self.IDLABORATORIO, item)

        # ejecutamos la consulta y retornamos el vector
        resultado = self.Link.consultar(consulta, parametros)
        return resultado

    def getDatosEgreso(self, idegreso):
        """Método que recibe como parámetro la clave de un egreso y retorna
           el vector con los datos del registro """

        # componemos la consulta
        consulta = ("SELECT diagnostico.v_egresos.idegreso AS idegreso, "
                    "       diagnostico.v_egresos.iditem AS iditem, "
                    "       diagnostico.v_egresos.descripcion AS item, "
                    "       diagnostico.v_egresos.codigosop AS codigosop, "
                    "       diagnostico.v_egresos.imagen AS imagen, "
                    "       diagnostico.v_egresos.remito AS remito, "
                    "       diagnostico.v_egresos.cantidad AS cantidad, "
                    "       diagnostico.v_egresos.nombre_entrego AS entrego, "
                    "       diagnostico.v_egresos.id_entrego AS identrego, "
                    "       diagnostico.v_egresos.nombre_recibio AS recibio, "
                    "       diagnostico.v_egresos.idrecibio AS idrecibio, "
                    "       diagnostico.v_egresos.fecha_egreso AS fecha, "
                    "       diagnostico.v_egresos.comentarios AS comentarios "
                    "FROM diagnostico.v_egresos "
                    "WHERE diagnostico.v_egresos.idegreso = %s;")

        # asignamos los parámetros
        parametros = (idegreso, )

        # obtenemos el registro y asignamos en las variables de clase
        resultado = self.Link.obtener(consulta, parametros)

        # asignamos en las variables de clase
        self.IdEgreso = resultado["idegreso"]
        self.ItemEgreso = resultado["item"]
        self.IdItemEgreso = resultado["iditem"]
        self.CodigoSop = resultado["codigosop"]
        self.Imagen = resultado["imagen"]
        self.RemitoEgreso = resultado["remito"]
        self.CantidadEgreso = resultado["cantidad"]
        self.EntregoEgreso = resultado["entrego"]
        self.IdEntregoEgreso = resultado["identrego"]
        self.RecibioEgreso = resultado["recibio"]
        self.IdRecibioEgreso = resultado["idrecibio"]
        self.FechaEgreso = resultado["fecha"]
        self.ComentariosEgreso = resultado["comentarios"]

    def grabaEgreso(self):
        """Método que a partir de las variables de clase ejecuta la consulta
           de inserción o actualización de un egreso 
           
           Retorna: idegreso - clave del registro afectado """

        # si esta insertando
        if self.IdEgreso == 0:
            self.nuevoEgreso()
        else:
            self.editaEgreso()

        # retornamos la id del registro
        return self.IdEgreso

    def nuevoEgreso(self):
        """ Método llamado al insertar un nuevo egreso """

        # componemos la consulta de insercion
        consulta = ("INSERT INTO diagnostico.egresos "
                    "       (item, "
                    "        id_laboratorio, "
                    "        remito, "
                    "        cantidad, "
                    "        id_entrego, "
                    "        id_recibio, "
                    "        comentarios) "
                    "       VALUES "
                    "       (%s, %s, %s, %s, %s, %s, %s);")

        # asignamos los parametros
        parametros = (self.ItemEgreso,
                      self.IDLABORATORIO,
                      self.RemitoEgreso,
                      self.CantidadEgreso,
                      self.EntregoEgreso,
                      self.RecibioEgreso,
                      self.ComentariosEgreso)

        # ejecutamos la consulta y obtenemos la id
        self.IdEgreso = self.Link.ejecutar(consulta, parametros)

    def editaEgreso(self):
        """ Método llamado al editar un egreso """

        # componemos la consulta de edicion
        consulta = ("UPDATE diagnostico.egresos SET "
                    "       item = %s, "
                    "       id_laboratorio = %s, "
                    "       remito = %s, "
                    "       cantidad = %s, "
                    "       id_entrego = %s, "
                    "       id_recibio = %s, "
                    "       comentarios = %s "
                    "WHERE diagnostico.egresos.id = %s;")

        # asignamos los parametros
        parametros = (self.ItemEgreso,
                      self.IDLABORATORIO,
                      self.RemitoEgreso,
                      self.CantidadEgreso,
                      self.EntregoEgreso,
                      self.RecibioEgreso,
                      self.ComentariosEgreso,
                      self.IdEgreso)

        # ejecutamos la consulta
        self.Link.ejecutar(consulta, parametros)

    def borraEgreso(self, idegreso):
        """Método que recibe como parámetro la id de un egreso, produce la
           consulta de eliminación y actualiza también el inventario y
           las tablas de auditoría """

        # componemos la consulta de eliminaciòn
        consulta = ("DELETE FROM diagnostico.egresos "
                    "WHERE diagnostico.egresos.id = %s;")
        parametros = (idegreso,)
        self.Link.ejecutar(consulta, parametros)
