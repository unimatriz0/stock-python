#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Nombre: dbapi.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 07/08/2017
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnostico
    Comentarios: clase que contiene los metodos para conectarse a la base
                 de datos y efectuar consultas de seleccion y edicion
                 en la misma

"""

# importamos la libreria
import pymysql


class Conexion():
    """ Constructor de la clase, no recibe parametros.
        Establece la conexion con la base de datos y
        hace disponible el puntero para el resto de
        la clase"""

    def __init__(self):
        """ Constructor de la clase """

        # definimos el estado
        estado = "Desarrollo"

        # segùn el estado
        if estado == "Desarrollo":

            # establecemos los parámetros de conexión
            servidor = "localhost"
            usuario = "claude"
            palabra = "gamaeco"
            base = "cce"

        # si está en desarrollo
        else:

            # establecemos los paràmetros
            servidor = "fatalachaben.info.tm"
            usuario = "claudefatala"
            palabra = "pickard47alfatango"
            base = "cce"

        # establecemos la conexion con la base
        self.CCE = pymysql.connect(host=servidor,
                                   user=usuario,
                                   password=palabra,
                                   db=base,
                                   charset='utf8')

        # seteamos la página de códigos de la conexión
        cursor = self.CCE.cursor()
        cursor.execute("SET CHARACTER_SET_CLIENT=utf8;")
        cursor.execute("SET CHARACTER_SET_RESULTS=utf8;")
        cursor.execute("SET COLLATION_CONNECTION=utf8_unicode_ci;")
        cursor.close()

    def consultar(self, consulta, parametros=None):
        """ Metodo que recibe como parametros una consulta y un
            array de parametros, ejecuta la consulta en la base
            y retorna el array de resultados
            Parametros:
            consulta, la cadena con la consulta
            parametros, por defecto nulo, un array con los parametros
            retorna: array con los resultados

            Excepciones, si la consulta es nula o vacia

            """

        # si la consulta esta vacia o es nula
        if consulta == "" or consulta is None:
            raise ValueError("La consulta no puede ser vacia")

        # abrimos el cursor con el argumento cursortype para que
        # retorne como un diccionario y usemos los nombres de
        # los campos
        cursor = self.CCE.cursor(pymysql.cursors.DictCursor)

        # si no se recibieron parámetros
        if parametros is None:
            cursor.execute(consulta)
        else:
            cursor.execute(consulta, parametros)

        # obtenemos todos los registros
        resultado = cursor.fetchall()

        # cerramos el cursor
        cursor.close()

        # retornamos el vector
        return resultado

    def ejecutar(self, consulta, parametros):
        """ Función que recibe como parámetros una consulta de actualización
            y los parámetros, la ejecuta y retorna la id del registro
            afectado """

        # abrimos el cursor
        cursor = self.CCE.cursor()

        # si la consulta o los parametros son nulos
        if consulta is None or parametros is None:
            texto = "El texto de la consulta y los parametros son obligatorios"
            raise ValueError(texto)

        # ejecutamos la consulta y actualizamos
        cursor.execute(consulta, parametros)
        self.CCE.commit()

        # obtenemos la id del registro (aquí probamos con un try
        # porque si eliminó la id del registro no existe)
        try:

            # tratamos de obtener la id
            id_insertado = cursor.lastrowid

        # si no pudo obtenerlo
        except:

            # lo asignamos a cero
            id_insertado = 0

        # cerramos el cursor
        cursor.close()

        # retornamos la id
        return id_insertado

    def obtener(self, consulta, parametros=None):
        """" Metodo que recibe una consulta y los parametros optativos
             la utilizamos en consultas en las cuales esperamos obtener
             un solo registro
             Lanza excepcion al no recibir la consulta """

        if consulta == "" or consulta is None:
            raise ValueError("El texto con la consulta es obligatorio")

        # abrimos el cursor para que retorne el nombre del campo
        # en vez del indice
        cursor = self.CCE.cursor(pymysql.cursors.DictCursor)

        # si no recibió parámetros
        if parametros is None:

            # ejecutamos la consulta sin parámetros
            cursor.execute(consulta)

        # si recibió parámetros
        else:

            # ejecutamos la consulta con parámetros
            cursor.execute(consulta, parametros)

        # obtenemos el registro
        registro = cursor.fetchone()

        # retornamos el registro
        return registro
