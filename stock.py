#!/usr/bin/python3
# -*- coding: utf-8 -*-


"""
    Nombre: form_stock.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 01/02/2019
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Comentarios: Clase que arma el formulario de selección de
                 actividades de la plataforma

"""

# para traducir los formularios de ui a py usamos
# pyuic5 input.ui -o output.py

# importamos las librerías del sistema, las librerías de diseño
# y la librería QT con la que manejamos los eventos
import sys
from PyQt5 import QtCore, QtGui, QtWidgets

# importamos las clases
from seguridad.nuevo_password import nuevoPassword
from seguridad.ingreso import ingreso
from deposito.articulos.form_articulos import formArticulos
from financiamiento.form_financiamiento import FormFinanciamiento
from deposito.inventario.form_inventario import FormInventario
from deposito.ingresos.form_ingresos import FormIngresos


class form_stock(QtWidgets.QMainWindow):
    ' Definición de la clase '

    def __init__(self):
        """ Definimos el constructor el método init que configura el
            formulario lo llamamos luego de instanciar las propiedades
            ya que cambia su tamaño y los controles si el usuario
            tiene acceso a edicción """

        # llama el constructor del padre
        super(form_stock, self).__init__()

        # definimos las constantes del usuario
        self.IDLABORATORIO = 0      # clave del laboratorio
        self.USUARIO = ""           # nombre del usuario
        self.IDUSUARIO = 0          # clave del usuario
        self.STOCK = "No"           # si está autorizado

        # pedimos el acceso
        ingreso(self)

    def initForm(self):
        """ Método que inicializa las propiedades del formulario """

        # si está autorizado
        if self.STOCK == "Si":
            self.setGeometry(350, 150, 476, 299)
        else:
            self.setGeometry(350, 150, 476, 299)

        # fijamos el título
        self.setWindowTitle('Control de Stock')
        self.setWindowIcon(QtGui.QIcon('recursos/logo_fatala.jpg'))

        # definimos la fuente de los labels y los controles
        fontTitulo = QtGui.QFont()
        fontTitulo.setFamily("DejaVu Sans")
        fontTitulo.setPointSize(14)
        fontTitulo.setBold(True)
        fontTitulo.setWeight(75)
        fontControl = QtGui.QFont()
        fontControl.setFamily("DejaVu Sans")
        fontControl.setPointSize(10)
        fontControl.setBold(False)

        # el título del formulario
        lTitulo = QtWidgets.QLabel("Seleccione la acción a realizar", self)
        lTitulo.setGeometry(30, 30, 401, 18)
        lTitulo.setFont(fontTitulo)

        # el botón de cambio de contraseña
        btnPassword = QtWidgets.QPushButton("&Password", self)
        btnPassword.setGeometry(30, 74, 130, 60)
        btnPassword.setToolTip("Cambia su contraseña de ingreso")
        btnPassword.setFont(fontControl)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("recursos/39.png"))
        btnPassword.setIcon(icon)
        btnPassword.setIconSize(QtCore.QSize(22, 22))

        # fijamos el evento del botón password
        btnPassword.clicked.connect(self.nuevoPassword)

        # el botón de inventario
        btnInventario = QtWidgets.QPushButton("&Inventario", self)
        btnInventario.setGeometry(170, 74, 130, 60)
        btnInventario.setToolTip("Resumen de la existencia en depósito")
        btnInventario.setFont(fontControl)
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("recursos/81.png"))
        btnInventario.setIcon(icon1)
        btnInventario.setIconSize(QtCore.QSize(22, 22))

        # fijamos el evento del botón inventario
        btnInventario.clicked.connect(self.verInventario)

        # el botón de pedidos
        btnPedidos = QtWidgets.QPushButton("&Pedidos", self)
        btnPedidos.setGeometry(310, 74, 130, 60)
        btnPedidos.setToolTip("Realizar pedidos de materiales")
        btnPedidos.setFont(fontControl)
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("recursos/Despertador.png"))
        btnPedidos.setIcon(icon2)
        btnPedidos.setIconSize(QtCore.QSize(22, 22))

        # si está habilitado
        if self.STOCK == "Si":

            # el botón de ingresos
            btnIngresos = QtWidgets.QPushButton("I&ngresos", self)
            btnIngresos.setGeometry(30, 144, 130, 60)
            btnIngresos.setToolTip("Ingresos de materiales al depósito")
            btnIngresos.setFont(fontControl)
            icon3 = QtGui.QIcon()
            icon3.addPixmap(QtGui.QPixmap("recursos/Entrega.png"))
            btnIngresos.setIcon(icon3)
            btnIngresos.setIconSize(QtCore.QSize(22, 22))

            # fijamos el evento del botón inventario
            btnIngresos.clicked.connect(self.verIngresos)

            # el botón de egresos
            btnEgresos = QtWidgets.QPushButton("&Egresos", self)
            btnEgresos.setGeometry(170, 144, 130, 60)
            btnEgresos.setToolTip("Entregas de materiales realizadas")
            btnEgresos.setFont(fontControl)
            icon4 = QtGui.QIcon()
            icon4.addPixmap(QtGui.QPixmap("recursos/salida.png"))
            btnEgresos.setIcon(icon4)
            btnEgresos.setIconSize(QtCore.QSize(22, 22))

            # el botón de artículos o items
            btnArticulos = QtWidgets.QPushButton("&Articulos", self)
            btnArticulos.setGeometry(310, 144, 130, 60)
            btnArticulos.setToolTip("Diccionario de artículos del depósito")
            btnArticulos.setFont(fontControl)
            icon5 = QtGui.QIcon()
            icon5.addPixmap(QtGui.QPixmap("recursos/editar_1.png"))
            btnArticulos.setIcon(icon5)
            btnArticulos.setIconSize(QtCore.QSize(22, 22))

            # fijamos el evento del botón password
            btnArticulos.clicked.connect(self.verArticulos)

            # el de fuentes de financiamiento
            btnFinanciamiento = QtWidgets.QPushButton("&Financiamiento", self)
            btnFinanciamiento.setGeometry(30, 214, 130, 60)
            btnFinanciamiento.setToolTip("Fuentes de financiamiento")
            btnFinanciamiento.setFont(fontControl)
            icon6 = QtGui.QIcon()
            icon6.addPixmap(QtGui.QPixmap("recursos/Porcentaje.png"))
            btnFinanciamiento.setIcon(icon6)
            btnFinanciamiento.setIconSize(QtCore.QSize(22, 22))

            # fijamos el evento del botón financiamiento
            btnFinanciamiento.clicked.connect(self.verFinanciamiento)

    def nuevoPassword(self):
        ' Método que presenta el formulario de nuevo password '

        # instanciamos la clase y asignamos la clave de usuario
        form_pass = nuevoPassword(self)
        form_pass.IdUsuario = self.IDUSUARIO
        form_pass.initUI()

    def verFinanciamiento(self):
        ' Método que presenta el formulario de fuentes de financiamiento '

        # instanciamos la clase y asignamos la clave de usuario
        form_fuentes = FormFinanciamiento(self)
        form_fuentes.IdUsuario = self.IDUSUARIO
        form_fuentes.IdLaboratorio = self.IDLABORATORIO
        form_fuentes.Usuario = self.USUARIO
        form_fuentes.setupUi()

    def verArticulos(self):
        ' Método que presenta el formulario de artículos '

        # instanciamos la clase y asignamos la clave de usuario
        form_articulos = formArticulos(self)
        form_articulos.IDUSUARIO = self.IDUSUARIO
        form_articulos.USUARIO = self.USUARIO
        form_articulos.IDLABORATORIO = self.IDLABORATORIO
        form_articulos.setupUi()

    def verInventario(self):
        ' Método que presenta el formulario de inventario'

        # instanciamos la clase y fijamos los argumentos
        inventario = FormInventario(self)
        inventario.IDLABORATORIO = self.IDLABORATORIO
        inventario.cargaInventario()
        inventario.show()

    def verIngresos(self):
        ' Método que presenta el formulario de ingresos '

        # instanciamos la clase
        ingresos = FormIngresos(self)
        ingresos.show()

# iniciamos la aplicación
app = QtWidgets.QApplication(sys.argv)
root = form_stock()
sys.exit(app.exec_())  # iniciamos el bucle principal