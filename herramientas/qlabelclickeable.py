#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""

    Nombre: qlabelclickeable.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 25/02/2019
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Comentarios: Clase que hereda la clase qlabel de pyqt5 y permite
                 hacer clickeable un label

"""

# importamos las librerías
from PyQt5.QtCore import pyqtSignal
from PyQt5.QtWidgets import QLabel


class QLabelClickeable(QLabel):
    """ Definición de la clase """

    # definimos la señal
    clicked = pyqtSignal()

    # constructor
    def __init__ (self, parent=None):
        super(QLabelClickeable, self).__init__(parent)

    # definimos el evento click del mouse
    def mousePressEvent(self, event):
        self.clicked.emit()
