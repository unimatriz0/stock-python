#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Nombre: nuevo_password.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 08/08/2017
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Comentarios: Procedimiento que arma el formulario de cambio de contraseña

"""

# importamos las librerías
# para traducir los formularios de ui a py usamos
# pyuic5 input.ui -o output.py

# importamos las librerías del sistema, las librerías de diseño
# y la librería QT con la que manejamos los eventos
from PyQt5 import QtCore, QtGui, QtWidgets

# importamos la librería de seguridad
from seguridad.seguridad import Seguridad


class nuevoPassword(QtWidgets.QDialog):
    'definimos la clase '

    def __init__(self, parent):
        """ definimos el constructor de la clase que recibe
            como parámetro el padre """

        # definimos la variable idUsuario
        self.IdUsuario = 0

        # llama al constructor del padre
        super(nuevoPassword, self).__init__(parent)

    def initUI(self):
        'Configuración del formulario'

        # redimensionamos y fijamos la posición del formulario
        self.setGeometry(370, 200, 400, 222)

        # fijamos el ícono
        self.setWindowIcon(QtGui.QIcon('recursos/logo_fatala.jpg'))

        # fijamos el título de la ventana
        self.setWindowTitle("Nueva Contraseña")

        # establecemos la ventana como modal
        self.setModal(True)

        # definimos las fuentes de los labels y los controles
        fontLabel = QtGui.QFont()
        fontLabel.setFamily("DejaVu Sans")
        fontLabel.setBold(True)
        fontLabel.setPointSizeF(10)
        fontControl = QtGui.QFont()
        fontControl.setFamily("DejaVu Sans")
        fontControl.setPointSize(10)
        fontControl.setBold(False)

        # presentamos el título
        titulo = "Ingrese sus datos y pulse el botón Aceptar para cambiar su contraseña de ingreso"
        lTitulo = QtWidgets.QLabel(titulo, self)
        lTitulo.setGeometry(20, 6, 360, 40)
        lTitulo.setFont(fontLabel)
        lTitulo.setWordWrap(True)

        # pedimos el password actual
        lPasswordActual = QtWidgets.QLabel("Contraseña Actual:", self)
        lPasswordActual.setGeometry(30, 51, 140, 14)
        lPasswordActual.setFont(fontLabel)
        self.tPasswordActual = QtWidgets.QLineEdit(self)
        self.tPasswordActual.setGeometry(174, 45, 100, 30)
        self.tPasswordActual.setMaxLength(16)
        self.tPasswordActual.setEchoMode(QtWidgets.QLineEdit.Password)
        self.tPasswordActual.setToolTip("Contraseña actual de conexión")
        self.tPasswordActual.setFont(fontControl)

        # pedimos el nuevo password
        lNuevoPassword = QtWidgets.QLabel("Nuevo Password: ", self)
        lNuevoPassword.setGeometry(38, 92, 125, 14)
        lNuevoPassword.setFont(fontLabel)
        self.tNuevoPassword = QtWidgets.QLineEdit(self)
        self.tNuevoPassword.setGeometry(174, 85, 100, 30)
        self.tNuevoPassword.setEchoMode(QtWidgets.QLineEdit.Password)
        self.tNuevoPassword.setToolTip("Nueva contraseña de conexión")
        self.tNuevoPassword.setFont(fontControl)

        # pedimos la repetición
        lRepeticion = QtWidgets.QLabel("Repetición: ", self)
        lRepeticion.setGeometry(84, 134, 90, 14)
        lRepeticion.setFont(fontLabel)
        self.tRepeticion = QtWidgets.QLineEdit(self)
        self.tRepeticion.setGeometry(174, 126, 100, 30)
        self.tRepeticion.setEchoMode(QtWidgets.QLineEdit.Password)
        self.tRepeticion.setToolTip("Repita la nueva contraseña para verificar")
        self.tRepeticion.setFont(fontControl)

        # agregamos el botón aceptar
        self.BtnAceptar = QtWidgets.QPushButton("&Aceptar", self)
        self.BtnAceptar.setGeometry(130, 180, 89, 30)
        self.BtnAceptar.setToolTip('Pulse para cambiar su contraseña')
        self.BtnAceptar.setFont(fontControl)

        # asignamos el ícono al botón
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("recursos/confirmation.png"))
        self.BtnAceptar.setIcon(icon1)
        self.BtnAceptar.setIconSize(QtCore.QSize(16, 16))

        # agregamos el botón cancelar
        self.BtnCancelar = QtWidgets.QPushButton("&Cancelar", self)
        self.BtnCancelar.setGeometry(240, 180, 89, 30)
        self.BtnCancelar.setToolTip('Cierra la ventana sin cambiar nada')
        self.BtnCancelar.setFont(fontControl)

        # asignamos el ícono al botón
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("recursos/Borrar1.png"))
        self.BtnCancelar.setIcon(icon2)
        self.BtnCancelar.setIconSize(QtCore.QSize(16, 16))

        # fijamos el evento del botón aceptar
        self.BtnAceptar.clicked.connect(self.cambiarPassword)

        # fijamos el evento del botón cancelar
        self.BtnCancelar.clicked.connect(self.cancelar)

        # fijamos el foco en el primer elemento
        self.tPasswordActual.setFocus()

        # mostramos el formulario
        self.show()

    def keyPressEvent(self, event):
        'Definición del evento keypress '

        # si pulsó enter
        if event.key() == QtCore.Qt.Key_Return or event.key() == QtCore.Qt.Key_Enter:

            # si el foco lo tiene el passwordactual
            if self.tPasswordActual.hasFocus():

                # setea el foco en la nueva contraseña
                self.tNuevoPassword.setFocus()

            # si el foco lo tiene la nueva contraseña
            elif self.tNuevoPassword.hasFocus():

                # setea el foco la repetición
                self.tRepeticion.setFocus()

            # si el foco lo tiene la repetición
            elif self.tRepeticion.hasFocus():

                # setea el foco en el botón
                self.BtnAceptar.setFocus()

            # si ya está en el botón
            else:

                # llama la rutina de ingreso
                self.cambiarPassword()

    def cambiarPassword(self):
        """Función llamada al pulsar sobre el botón aceptar,
           verifica los datos del formulario y luego actualiza
           en la base de datos """

        # instancia la clase
        actualizar = Seguridad()

        # verificamos que halla ingresado la contraseña actual
        if str(self.tPasswordActual.text()) == "":

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = "Debe ingresar su contraseña\n"
            mensaje += "actual de conexión"
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # verificamos que halla ingresado la nueva contraseña
        elif str(self.tNuevoPassword.text()) == "":

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = "Debe ingresar su la nueva\n"
            mensaje += "contraseña de ingreso"
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # verificamos que la nueva y la repetición coinciden
        elif self.tNuevoPassword.text() != self.tRepeticion.text():

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = "La nueva contraseña y su\n"
            mensaje += "repetición no coinciden"
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # verificamos la longitud de la nueva contraseña
        elif len(self.tNuevoPassword.text()) < 6:

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = "La contraseña debe tener al menos\n"
            mensaje += "seis(6) caracteres por seguridad"
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # verificamos que la contraseña actual sea correcta
        elif not actualizar.validaPassword(self.IdUsuario, self.tNuevoPassword.text()):

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = "La contraseña actual no coincide con\n"
            mensaje += "la declarada en la base de datos"
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # si llegó hasta aquí
        else:

            # actualizamos el password pasándole los argumentos
            actualizar.nuevoPassword(self.IdUsuario,
                                     str(self.tNuevoPassword.text()))

            # presenta el mensaje
            titulo = 'Contraseña actualizada'
            mensaje = "La contraseña ha sido actualizada con éxito\n"
            mensaje += "recuerde utilizarla la próxima vez."
            QtWidgets.QMessageBox.information(self, titulo, mensaje)

            # cierra la ventana
            self.close()

    def cancelar(self):
        ' función llamada al pulsar sobre el botón cancelar '

        # cerramos la ventana
        self.close()
