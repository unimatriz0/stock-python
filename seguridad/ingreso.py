#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Nombre: ingreso.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 01/02/2019
    E-Mail: cinvernizzi@gmail.com
    Licencia: GPL
    Proyecto: Diagnóstico
    Comentarios: Procedimiento que arma la pantalla de ingreso al sistema y
                 luego de acreditarse, muestra el formulario de opciones de
                 stock

"""

# para traducir los formularios de ui a py usamos
# pyuic5 input.ui -o output.py

# importamos las librerías del sistema, las librerías de diseño
# y la librería QT con la que manejamos los eventos
from PyQt5 import QtCore, QtGui, QtWidgets

# importamos las librerías
from seguridad.seguridad import Seguridad

# importamos la definición del formulario principal


class ingreso(QtWidgets.QDialog):
    'definimos la clase de la ventana'

    def __init__(self, parent):
        """constructor de la clase """

        # llama el constructor del padre
        super(ingreso, self).__init__(parent)
        self.Padre = parent

        # inicializamos el formulario
        self.initUI()

    def initUI(self):
        """ función de inicialización del formulario de ingreso """

        # fijamos el título y la geometría de la ventana
        self.setGeometry(350, 150, 476, 299)
        self.setWindowTitle('Sistema de Diagnóstico')
        self.setWindowIcon(QtGui.QIcon('recursos/logo_fatala.jpg'))

        # definimos las fuentes de los labels y los controles
        fontLabel = QtGui.QFont()
        fontLabel.setFamily("DejaVu Sans")
        fontLabel.setBold(True)
        fontLabel.setPointSizeF(10)
        fontControl = QtGui.QFont()
        fontControl.setFamily("DejaVu Sans")
        fontControl.setPointSize(10)
        fontControl.setBold(False)

        # fijamos el título de la página
        lTitulo = QtWidgets.QLabel("Instituto Nacional de Parasitología", self)
        lTitulo.setGeometry(78, 10, 321, 51)
        lTitulo.setFont(fontLabel)
        lTitulo.setWordWrap(True)

        # fijamos el subtítulo
        lPrograma = QtWidgets.QLabel("Sistema de Diagnóstico y Seguimiento", self)
        lPrograma.setGeometry(55, 62, 441, 21)
        lPrograma.setFont(fontLabel)

        # asignamos el texto introductorio y lo presentamos
        texto = "Ingrese sus credenciales para ingresar a la plataforma, "
        texto += "recuerde que en el foro http://fatalachaben.info.tm/foro/ "
        texto += "del Programa puede solicitar asistencia"
        lTexto = QtWidgets.QLabel(texto, self)
        lTexto.setGeometry(14, 90, 441, 61)
        lTexto.setWordWrap(True)
        lTexto.setFont(fontControl)

        # presentamos el label de usuario
        lUsuario = QtWidgets.QLabel("Nombre de Usuario: ", self)
        lUsuario.setGeometry(170, 180, 151, 16)
        lUsuario.setFont(fontLabel)

        # pedimos el nombre de usuario
        self.tUsuario = QtWidgets.QLineEdit(self)
        self.tUsuario.setGeometry(320, 171, 101, 31)
        self.tUsuario.setMaxLength(16)
        self.tUsuario.setToolTip("Su Nombre de Usuario")
        self.tUsuario.setFont(fontControl)

        # presentamos el label del password
        lPassword = QtWidgets.QLabel("Contraseña: ", self)
        lPassword.setGeometry(223, 217, 101, 16)
        lPassword.setFont(fontLabel)

        # pedimos la contraseña
        self.tPassword = QtWidgets.QLineEdit(self)
        self.tPassword.setGeometry(320, 210, 101, 31)
        self.tPassword.setMaxLength(16)
        self.tPassword.setEchoMode(QtWidgets.QLineEdit.Password)
        self.tPassword.setToolTip("Su contraseña de ingreso")
        self.tPassword.setFont(fontControl)

        # presentamos el logo del fatala
        logo = QtWidgets.QLabel(self)
        logo.setGeometry(22, 159, 121, 111)
        pixmap = QtGui.QPixmap("recursos/logo_fatala.jpg")
        logo.setPixmap(pixmap)

        # definimos el botón ingresar
        self.BtnIngresar = QtWidgets.QPushButton("Ingresar", self)
        self.BtnIngresar.setGeometry(327, 255, 89, 31)
        self.BtnIngresar.setToolTip('Pulse para verificar sus credenciales')
        self.BtnIngresar.setFont(fontControl)

        # asignamos el ícono al botón
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("recursos/mconfirmar.png"))
        self.BtnIngresar.setIcon(icon1)

        # probamos de fijar el foco
        self.tUsuario.setFocus()

        # fijamos el evento del botón ingresar
        self.BtnIngresar.clicked.connect(self.Ingresar)

        # mostramos el formulario
        self.show()

    def keyPressEvent(self, event):
        """ Define el evento keypress que determina el campo que
            tiene el foco y avanza al siguiente campo si se
            pulsó enter """

        # si pulsó enter
        if event.key() == QtCore.Qt.Key_Return or event.key() == QtCore.Qt.Key_Enter:

            # si el foco lo tiene el usuario
            if self.tUsuario.hasFocus():

                # setea el foco en la contraseña
                self.tPassword.setFocus()

            # si el foco lo tiene la contraseña
            elif self.tPassword.hasFocus():

                # setea el foco en el botón
                self.BtnIngresar.setFocus()

            # si ya está en el botón
            else:

                # llama la rutina de ingreso
                self.Ingresar()

    def Ingresar(self):
        """ Evento llamado al pulsar el botón ingresar que
            verifica los datos del formulario y luego
            valida el usuario y contraseña, si son válidos
            muestra el formulario principal y fija los valores
            en el formulario padre """

        # verifica se halla ingresado el nombre de usuario
        if str(self.tUsuario.text()) == "":

            # presenta el mensaje
            titulo = 'Atención'
            QtWidgets.QMessageBox.warning(self, titulo, "Ingrese el nombre de usuario")
            return

        # verifica se halla ingresado la contraseña
        elif str(self.tPassword.text()) == "":

            # presenta el mensaje
            titulo = 'Atención'
            mensaje = 'Ingrese su contraseña'
            QtWidgets.QMessageBox.warning(self, titulo, mensaje)
            return

        # si ingresó ambos
        else:

            # verificamos las credenciales y en caso afirmativo
            # obtenemos la id del usuario para mostrar el registro
            usuario = str(self.tUsuario.text())
            password = str(self.tPassword.text())
            Ingreso = Seguridad()

            # verificamos las credenciales y obtenemos un array
            # con los datos del usuario
            usuario = Ingreso.VerificarUsuario(usuario, password)

            # si no pudo acreditar
            if not usuario:

                # presenta el mensaje
                titulo = 'Atención'
                mensaje = 'Verifique los datos ingresados'
                QtWidgets.QMessageBox.warning(self, titulo, mensaje)

                # fijamos el foco
                self.tUsuario.setFocus()

                # retornamos
                return

            # si autenticó
            else:

                # asignamos los valores en las variables
                # del formulario padre, mostramos el
                # formulario y cerramos este
                self.Padre.USUARIO = usuario["usuario"]
                self.Padre.IDUSUARIO = usuario["id"]
                self.Padre.IDLABORATORIO = usuario["laboratorio"]
                self.Padre.STOCK = usuario["stock"]
                self.Padre.initForm()
                self.Padre.show()
                self.close()
