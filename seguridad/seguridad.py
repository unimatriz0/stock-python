#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""
    Nombre: seguridad.py
    Autor: Lic. Claudio Invernizzi
    Fecha: 07/08/2017
    E-Mail: cinvernizzi@gmail.com
    Proyecto: Diagnostico
    Comentarios: Clase que contiene los métodos para el acceso de
                 los usuarios autorizados

"""

# importamos la clase de base de datos
from clases.dbapi import Conexion


class Seguridad:
    """Definición de la clase"""

    def __init__(self):
        ' constructor de la clase '

        # instanciamos la conexión
        self.Link = Conexion()

    def VerificarUsuario(self, usuario, password):
        """ Función que recibe como parámetros el usuario y la
            contraseña, retorna verdadero si coincide y falso
            en caso contrario """

        # verificamos los parámetros
        if usuario is None or password is None:
            raise ValueError("Debe indicar el usuario y el password")

        # componemos la consulta buscando el usuario
        # y y verificamos que coincida con la contraseña encriptada
        consulta = ("SELECT diagnostico.v_usuarios.idusuario AS id, "
                    "       diagnostico.v_usuarios.nombreusuario AS nombre, "
                    "       diagnostico.v_usuarios.usuario AS usuario, "
                    "       diagnostico.v_usuarios.idlaboratorio AS laboratorio, "
                    "       diagnostico.v_usuarios.codprov AS provincia, "
                    "       diagnostico.v_usuarios.idpais AS pais, "
                    "       diagnostico.v_usuarios.codloc AS localidad, "
                    "       diagnostico.v_usuarios.nivelcentral AS nivel_central, "
                    "       diagnostico.v_usuarios.administrador AS administrador, "
                    "       diagnostico.v_usuarios.personas AS personas, "
                    "       diagnostico.v_usuarios.congenito AS congenito, "
                    "       diagnostico.v_usuarios.resultados AS resultados, "
                    "       diagnostico.v_usuarios.muestras AS muestras, "
                    "       diagnostico.v_usuarios.entrevistas AS entrevistas, "
                    "       diagnostico.v_usuarios.auxiliares AS auxiliares, "
                    "       diagnostico.v_usuarios.protocolos AS protocolos, "
                    "       diagnostico.v_usuarios.stock AS stock, "
                    "       diagnostico.v_usuarios.usuarios AS usuarios "
                    "FROM diagnostico.v_usuarios "
                    "WHERE diagnostico.v_usuarios.usuario = %s AND "
                    "      diagnostico.v_usuarios.password = MD5(%s) AND "
                    "      diagnostico.v_usuarios.activo = 'Si';")

        # asignamos los parámetros
        parametros = (usuario, password)

        # ejecutamos la consulta usando la api y obtenemos
        # el password de la base
        resultado = self.Link.obtener(consulta, parametros)

        # si no encontró el usuario
        if resultado is None:

            # retorna falso
            return False

        # si obtuvo registros
        else:

            # retorna el array
            return resultado

    def nuevoPassword(self, idUsuario, password):
        """ Método que recibe como parámetro la id del usuario y la
            nueva contraseña de ingreso, genera la consulta de
            actualización en la base """

        # componemos la consulta
        consulta = ("UPDATE cce.responsables SET "
                    "       cce.responsables.PASSWORD = MD5(%s) "
                    "WHERE cce.responsables.ID = %s;")
        parametros = (password, idUsuario)

        # ejecutamos la consulta
        self.Link.ejecutar(consulta, parametros)

        # retornamos
        return

    def validaPassword(self, idusuario, password):
        """ Método que recibe como parámetrola id de usuario
            y la contraseña actual y verifica que coincidan """

        # componemos la consulta
        consulta = ("SELECT COUNT(cce.responsables.id) AS registros "
                    "FROM cce.responsables "
                    "WHERE cce.responsables.id = %s AND "
                    "      cce.responsables.password = MD5(%s); ")

        # establece los parámetros
        parametros = (idusuario, password)

        # ejecutamos la consulta y obtenemos el registro
        resultado = self.Link.obtener(consulta, parametros)

        # si hubo registros
        if resultado["registros"] != 0:
            return True
        else:
            return False
